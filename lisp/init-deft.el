;;; init-deft.el --- Settings for deft -*- lexical-binding: t -*-
;;; Commentary:
;;; Code:

(use-package deft
  :after org-roam
  :bind
  ("C-c n d" . deft)
  :config
  (setq deft-recursive t)
  (setq deft-use-filter-string-for-filename t)
  (setq deft-default-extension "org")
  (setq deft-directory org-roam-directory))

(provide 'init-deft)
;;; init-deft.el ends here
