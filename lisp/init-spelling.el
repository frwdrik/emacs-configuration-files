;;; init-spelling.el --- Spell check settings -*- lexical-binding: t -*-
;;; Commentary:
;;; Code:

(use-package ispell
  :ensure t
  :defer t) ;; otherwise ispell-program-name undefined

(use-package flyspell
  :requires ispell
  :if (executable-find ispell-program-name)
  :hook (prog-mode . flyspell-prog-mode)
  :config (add-to-list 'flyspell-prog-text-faces 'nxml-text-face))

(provide 'init-spelling)
;;; init-spelling.el ends here
