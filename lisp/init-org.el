;;; init-org.el --- Org-mode config -*- lexical-binding: t -*-
;;; Commentary: This file is a mess. Let's clean it up. Smooth typing is the best.
;;; Code:

(use-package xdg)
(use-package org-cliplink
  :ensure t)
(use-package org
  :config
  (custom-set-faces
   '(org-level-1 ((t (:inherit outline-1 :height 1.2))))
   '(org-level-2 ((t (:inherit outline-2 :height 1.1))))
   '(org-level-3 ((t (:inherit outline-3 :height 1.0))))
   '(org-level-4 ((t (:inherit outline-4 :height 1.0))))
   '(org-level-5 ((t (:inherit outline-5 :height 1.0)))))
  (auto-fill-mode 1))
(use-package org-ref
  :ensure t
  :diminish t
  :config
  (require 'tex)
  ;; bibtex-completion-bibliography is defined in init-latex.org
  (setq org-ref-default-bibliography
	(list
	 (--if-let bibtex-completion-bibliography
	     it
	   (file-truename "~/imperial/bibliography.bib"))))
  (setq org-ref-pdf-directory bibtex-completion-library-path)
  (org-ref-ivy-cite-completion)	 ; use ivy as frontend, C-c ]
  )

(use-package org-download :ensure t)

;; Make LaTeX previews in org files slightly larger
(plist-put org-format-latex-options :scale 1.4)

(require 'init-org-latex-header)
;; (defconst old-org-format-latex-header org-format-latex-header)
(setq org-format-latex-header
      (s-concat old-org-latex-header extra-org-latex-header))
;;(setq org-latex-package-alist '(("" "")))

;; Custom Key Bindings
(global-set-key (kbd "<f12>") 'org-agenda)
(global-set-key (kbd "<f11>") 'org-clock-goto)
(global-set-key (kbd "C-<f11>") 'org-clock-in)
(global-set-key (kbd "<f8>") 'org-cycle-agenda-files)
(global-set-key (kbd "<f9> <f9>") 'bh/show-org-agenda)

(global-set-key (kbd "<f9> c") 'calendar)

(define-key global-map (kbd "C-c l") 'org-store-link)
(define-key global-map (kbd "C-c a") 'org-agenda)
(define-key global-map (kbd "C-c c") 'org-capture)


;; Various preferences
(setq org-log-done t
      org-support-shift-select 'always
      org-treat-S-cursor-todo-selection-as-state-change nil
      org-list-allow-alphabetical t
      org-edit-timestamp-down-means-later t
      org-hide-emphasis-markers t
      org-catch-invisible-edits 'show
      org-export-coding-system 'utf-8
      org-fast-tag-selection-single-key 'expert
      org-html-validation-link nil
      org-export-kill-product-buffer-when-displayed t
      org-tags-column 80
      org-startup-indented t
      org-confirm-babel-evaluate nil   ; no confirmation on C-c C-c
      org-journal-enable-agenda-integration t
      org-directory (concat (file-name-as-directory
			     (xdg-user-dir "DOCUMENTS"))
			    "org")
      org-default-notes-file (concat (file-name-as-directory
				      (xdg-user-dir "DOCUMENTS"))
				     "org/notes")
      org-clock-sound (concat (xdg-data-home) "/sounds/ALARM.WAV")
      org-agenda-files (list (concat (xdg-user-dir "DOCUMENTS") "/org/")
			     "~/imperial/phd.org"))

(diminish 'org-indent-mode)

;; Show bullets instead of asterisks
(use-package org-bullets
  :ensure t
  :hook (org-mode . org-bullets-mode))

(use-package org-roam
  :ensure t
  :diminish
  :hook (after-init . org-roam-mode)
  :bind (:map org-roam-mode-map
              (("C-c n l" . org-roam)
               ("C-c n f" . org-roam-find-file)
               ("C-c n g" . org-roam-graph-show)
	       ("C-c n A" . org-roam-add-alias)
	       ("C-c n k" . org-roam-add-key)
               ("C-c n a" . org-roam-add-tags)
               ("C-c n n" . orb-note-actions))
              :map org-mode-map
              (("C-c n i" . org-roam-insert))
              (("C-c n I" . org-roam-insert-immediate)))
  :config
  (setq org-roam-directory
	(concat (file-name-as-directory
		 (xdg-data-home))
		"emacs/"))
  (setq org-roam-verbose nil)
  (setq org-roam-capture--header-default "#+TITLE: ${title}\n")
  (setq org-roam-capture-templates
	'(("d" "default" plain (function org-roam-capture--get-point)
	   "%?"
	   :file-name "%<%Y%m%d%H%M%S>-${slug}"
	   :head "#+TITLE: ${title}\n"
	   :unnarrowed t)))
  (require 'org-roam-protocol)

  (add-hook 'org-roam-capture-after-find-file-hook
	    (lambda ()
	      (setq org-roam-current-title
		    (car (org-roam--extract-titles)))
	      (save-buffer)
	      (org-roam-db--update-file)
	      (org-roam-buffer--update-maybe :redisplay t)))

  ;; Customizing some functions
  (defun org-roam--handle-title-change ()
    "Detect a title change, and run `org-roam-title-change-hook'."
    (let ((new-title (car (org-roam--extract-titles)))
          (old-title org-roam-current-title))
      (unless (or (eq old-title nil)
                  (string-equal old-title new-title))
	(run-hook-with-args 'org-roam-title-change-hook old-title new-title)
	(setq-local org-roam-current-title new-title)
	(org-roam-buffer--update-maybe :redisplay t))))
  
  (defun org-roam--update-file-name-on-title-change (old-title new-title)
    "Update the file name on title change.
The slug is computed from OLD-TITLE using
`org-roam-title-to-slug-function'. If the slug is part of the
current filename, the new slug is computed with NEW-TITLE, and
that portion of the filename is renamed.

To be added to `org-roam-title-change-hook'."
    (when org-roam-rename-file-on-title-change
      (let* ((old-slug (funcall org-roam-title-to-slug-function old-title))
             (file (buffer-file-name (buffer-base-buffer)))
             (file-name (file-name-nondirectory file)))
	(when (string-match-p old-slug file-name)
          (let* ((new-slug (funcall org-roam-title-to-slug-function new-title))
		 (new-file-name (replace-regexp-in-string old-slug new-slug file-name)))
            (set-visited-file-name new-file-name t t)
	    (rename-file file-name new-file-name)
	    (add-to-list 'org-roam--file-update-queue new-file-name)
            (org-roam-message "File moved to %S" (abbreviate-file-name new-file-name)))))))

  (defun org-roam-insert (&optional lowercase completions filter-fn description link-type)
    "Find an Org-roam file, and insert a relative org link to it at point.
Return selected file if it exists.
LINK-TYPE is the type of link to be created. It defaults to \"file\".
If LOWERCASE, downcase the title before insertion.
COMPLETIONS is a list of completions to be used instead of
`org-roam--get-title-path-completions`.
FILTER-FN is the name of a function to apply on the candidates
which takes as its argument an alist of path-completions.
If DESCRIPTION is provided, use this as the link label.  See
`org-roam--get-title-path-completions' for details."
    (interactive "P")
    (unless org-roam-mode (org-roam-mode))
    ;; Deactivate the mark on quit since `atomic-change-group' prevents it
    (unwind-protect
	;; Group functions together to avoid inconsistent state on quit
	(atomic-change-group
          (let* (region-text
		 beg end
		 (_ (when (region-active-p)
                      (setq beg (set-marker (make-marker) (region-beginning)))
                      (setq end (set-marker (make-marker) (region-end)))
                      (setq region-text (buffer-substring-no-properties beg end))))
		 (completions (--> (or completions
                                       (org-roam--get-title-path-completions))
                                   (if filter-fn
                                       (funcall filter-fn it)
                                     it)))
		 (title-with-tags (org-roam-completion--completing-read "File: " completions
									:initial-input region-text))
		 (res (cdr (assoc title-with-tags completions)))
		 (title (or (plist-get res :title)
                            title-with-tags))
		 (target-file-path (plist-get res :path))
		 (description (or description region-text title))
		 (link-description (read-string "Description: "
						(org-roam--format-link-title (if lowercase
										 (downcase description)
									       description)
									     link-type))))
            (cond ((and target-file-path
			(file-exists-p target-file-path))
                   (when region-text
                     (delete-region beg end)
                     (set-marker beg nil)
                     (set-marker end nil))
                   (insert (org-roam--format-link target-file-path link-description link-type)))
                  (t
                   (let ((org-roam-capture--info `((title . ,title-with-tags)
                                                   (slug . ,(funcall org-roam-title-to-slug-function title-with-tags))))
			 (org-roam-capture--context 'title))
                     (setq org-roam-capture-additional-template-props (list :region (org-roam-shield-region beg end)
                                                                            :insert-at (point-marker)
                                                                            :link-type link-type
                                                                            :link-description link-description
                                                                            :finalize 'insert-link))
                     (org-roam-capture--capture))))
            res))
      (deactivate-mark))))

(use-package hydra :ensure t)
(use-package org-fc
  :load-path "/home/frwdrik/.config/emacs/site-lisp/org-fc"
  :config
  (require 'org-fc-keymap-hint)
  (require 'org-fc-hydra)
  (global-set-key (kbd "C-c f") 'org-fc-hydra/body)

  (require 'org-roam)
  (setq org-fc-directories
	(or (list org-roam-directory)
	    (concat (file-name-as-directory
		     (xdg-data-home))
		    "emacs/")	    
	    ;;(expand-file-name "site-lisp/org-fc/" user-emacs-directory)
	    ))

  (defun org-fc-review-tag (tags)
    "Start a review session for TAGS.
TAGS is a list of tag names."
    (interactive
     (list (s-split " "
		    (completing-read "Enter tags to review (space sep): " nil)
		    t)))
    (let ((tag-filter `(:filter (or
				 ,@(mapcar 
				    (lambda (tag) (list 'tag tag))
				    tags)))))
      ;;(message "%s" tag-filter)
      (org-fc-review (append '(:paths all) tag-filter)))))

(defun org-roam-add-alias ()
  "Add a #+ROAM_ALIAS property to a note."
  (interactive)
  (goto-char (point-min))
  ;; Check if tags already exist
  (unless (re-search-forward (rx bol "#+ROAM_ALIAS: ") nil t)
    ;; Skip over other properties
    (re-search-forward "TITLE" nil t)
    (newline-at-end-of-line)
    (if (char-equal (char-after (1+ (point))) 10 )     
	(delete-char 1))
    (insert "#+ROAM_ALIAS: ")))

(defun org-roam-add-key ()
  "Add a #+ROAM_KEY property to a note."
  (interactive)
  (goto-char (point-min))
  ;; Check if tags already exist
  (unless (re-search-forward (rx bol "#+ROAM_KEY: ") nil t)
    ;; Skip over other properties
    (dolist (s '("TITLE" "ROAM_ALIAS"))
      (re-search-forward s nil t))
    (newline-at-end-of-line)
    (if (char-equal (char-after (1+ (point))) 10 )     
	(delete-char 1))
    (insert "#+ROAM_KEY: ")))
(defun org-roam-add-tags ()
  "Add a #+ROAM_TAGS property to a note."
  (interactive)
  (goto-char (point-min))
  ;; Check if tags already exist
  (unless (re-search-forward (rx bol "#+ROAM_TAGS: ") nil t)
    ;; Skip over other properties
    (dolist (s '("TITLE" "ROAM_KEY" "ROAM_ALIAS"))
      (re-search-forward s nil t))
    (newline-at-end-of-line)
    (if (char-equal (char-after (1+ (point))) 10 )     
	(delete-char 1))
    (insert "#+ROAM_TAGS: ")))

(use-package org-roam-bibtex
  :ensure t
  :diminish
  :config
  (setq orb-templates
	'(("r" "ref" plain
	   #'org-roam-capture--get-point
	   ""
	   :file-name "${citekey}"
	   :head "#+TITLE: Notes on: ${author-abbrev} (${year}): ${title}\n#+ROAM_KEY: ${ref}
" :unnarrowed t)))
  (push "year" orb-preformat-keywords)
  (org-roam-bibtex-mode 1)
  ;; orb uses an override advice to replace
  ;; bibtex-completion-edit-notes with orb-edit-notes-ad, and from
  ;; there it implements a capture system on top of org-roam-capture
  ;; (which sits ontop of org-capture. I know...). The great thing
  ;; about orb's capture is that by adding keywords to
  ;; orb-preformat-keywords, we can use these keywords in our
  ;; templates and they will be filled in with data from an
  ;; entry's bibtex field.
  ;;
  ;; orb-edit-notes-ad dynamically binds org-roam-capture-templates to
  ;; orb-templates.
  )					

(use-package company-org-roam :ensure t)

(defun fred-setup-org ()
  (set (make-local-variable 'company-backends)
       '(company-files
	 ;;company-capf
	 company-dabbrev
	 company-dabbrev-code
	 ;; company-keywords
	 ;; company-etags
	 ))
  (push 'company-org-roam company-backends)
  (auto-fill-mode 1))
(add-hook 'org-mode-hook 'fred-setup-org)

;; Lots of stuff from http://doc.norang.ca/org-mode.html

;; ;; Re-align tags when window shape changes
;; (after-load 'org-agenda
;;   (add-hook 'org-agenda-mode-hook
;;             (lambda () (add-hook 'window-configuration-change-hook 'org-agenda-align-tags nil t))))



;; (require 'ox-publish)
;; (setq org-publish-project-alist
;;       '(
;; 	("org-notes"
;; 	 :base-directory "~/src/org/web"
;; 	 :base-extension "org"
;; 	 :publishing-directory "~/src/public_html/"
;; 	 :recursive t
;; 	 :publishing-function org-html-publish-to-html
;; 	 :headline-levels 4             ; Just the default for this project.
;; 	 :auto-preamble t
;; 	 )
;; 	("org-static"
;; 	 :base-directory "~/src/org/web/"
;; 	 :base-extension "css\\|js\\|png\\|jpg\\|gif\\|pdf\\|mp3\\|ogg\\|swf"
;; 	 :publishing-directory "~/src/public_html/"
;; 	 :recursive t
;; 	 :publishing-function org-publish-attachment
;; 	 )
;; 	("org" :components ("org-notes" "org-static"))
;; 	))



(use-package org-journal
  :ensure t
  :demand     ; override the lazy loading caused by :bind
  :bind ("C-c j" . org-journal-new-entry)
  ;; Must set org-journal-dir before loading org-journal
  :init (setq org-journal-dir (concat (file-name-as-directory
				       (xdg-data-home))
				      "emacs/journal"))
  :config
  (setq org-journal-file-format "%Y-%m-%d"))

(use-package writeroom-mode
  :ensure t)

(define-minor-mode prose-mode
  "Set up a buffer for prose editing.
This enables or modifies a number of settings so that the
experience of editing prose is a little more like that of a
typical word processor."
  nil " Prose" nil
  (if prose-mode
      (progn
        (when (fboundp 'writeroom-mode)
          (writeroom-mode 1))
        (setq truncate-lines nil)
        (setq word-wrap t)
        (setq cursor-type 'bar)
        (when (eq major-mode 'org)
          (kill-local-variable 'buffer-face-mode-face))
        (buffer-face-mode 1)
        (delete-selection-mode 1)
        (set (make-local-variable 'blink-cursor-interval) 0.6)
        (set (make-local-variable 'show-trailing-whitespace) nil)
        (set (make-local-variable 'line-spacing) 0.2)
        (set (make-local-variable 'electric-pair-mode) nil)
        (ignore-errors (flyspell-mode 1))
        (visual-line-mode 1))
    (kill-local-variable 'truncate-lines)
    (kill-local-variable 'word-wrap)
    (kill-local-variable 'cursor-type)
    (kill-local-variable 'blink-cursor-interval)
    (kill-local-variable 'show-trailing-whitespace)
    (kill-local-variable 'line-spacing)
    (kill-local-variable 'electric-pair-mode)
    (buffer-face-mode -1)
    ;; (delete-selection-mode -1)
    (flyspell-mode-off)
    (visual-line-mode -1)
    (when (fboundp 'writeroom-mode)
      (writeroom-mode 0))))

(add-hook 'org-mode-hook 'buffer-face-mode)




;;; Capturing

(global-set-key (kbd "C-c c") 'org-capture)

;; (setq org-capture-templates
;;       `(("t" "todo" entry (file "")  ; "" => `org-default-notes-file'
;;          "* NEXT %?\n%U\n" :clock-resume t)
;;         ("n" "note" entry (file "")
;;          "* %? :NOTE:\n%U\n%a\n" :clock-resume t)
;;         ))

(defun make-link-or-cliplink ()
  (let ((link (org-cliplink-clipboard-content)))
    (if (member (url-type (url-generic-parse-url link))
		'("http" "https"))
	(org-cliplink))))

;; Capture templates for: TODO tasks, Notes, appointments, phone calls, meetings, and org-protocol
(setq org-capture-templates
      (quote (("t" "todo" entry (file "refile.org")
               "* TODO %?\n%U\n%a\n" :clock-in t :clock-resume t)
              ("n" "note" entry (file "refile.org")
               "* %? :NOTE:\n%U\n%a\n" :clock-in t :clock-resume t)
              ("r" "Read article" entry (file "refile.org")
               "* %? \n%U\n%(org-cliplink-capture)" :clock-in t :clock-resume t)
              ("j" "Journal" entry (file+olp+datetree "diary.org")
               "* %?\n%U\n" :clock-in t :clock-resume t)
              ("T" "Training" entry (file+olp+datetree "trening.org")
               "* %?
:PROPERTIES:
:TIME:    \n:END:
%U" :clock-in nil)
              ("h" "Habit" entry (file "refile.org")
               "* NEXT %?\n%U\n%a\nSCHEDULED: %(format-time-string \"%<<%Y-%m-%d %a .+1d/3d>>\")\n:PROPERTIES:\n:STYLE: habit\n:REPEAT_TO_STATE: NEXT\n:END:\n"))))

;; Remove empty LOGBOOK drawers on clock out
(defun fred-remove-empty-drawer-on-clock-out ()
  (interactive)
  (save-excursion
    (beginning-of-line nil)
    (org-remove-empty-drawer-at (point))))

(add-hook 'org-clock-out-hook 'fred-remove-empty-drawer-on-clock-out 'append)


;;; Refiling

;; Exclude DONE state tasks from refile targets
(defun fred-verify-refile-target ()
  "Exclude todo keywords with a done state from refile targets"
  (not (member (nth 2 (org-heading-components)) org-done-keywords)))

(setq org-refile-target-verify-function 'fred-verify-refile-target)

;; Use full outline paths for refile targets - we file directly with IVY
(setq org-refile-use-outline-path 'full-file-path)

;; (setq org-refile-use-cache nil)

;; Targets include this file and any file contributing to the agenda - up to 5 levels deep
(setq org-refile-targets '((nil :maxlevel . 5) (org-agenda-files :maxlevel . 5)))
(setq org-outline-path-complete-in-steps nil)
;; Allow refile to create parent tasks with confirmation
(setq org-refile-allow-creating-parent-nodes (quote confirm))

;; (after-load 'org-agenda
;;   (add-to-list 'org-agenda-after-show-hook 'org-show-entry))

;; (advice-add 'org-refile :after (lambda (&rest _) (org-save-all-org-buffers)))

;; ;; Exclude DONE state tasks from refile targets
;; (defun sanityinc/verify-refile-target ()
;;   "Exclude todo keywords with a done state from refile targets."
;;   (not (member (nth 2 (org-heading-components)) org-done-keywords)))
;; (setq org-refile-target-verify-function 'sanityinc/verify-refile-target)

;; (defun sanityinc/org-refile-anywhere (&optional goto default-buffer rfloc msg)
;;   "A version of `org-refile' which allows refiling to any subtree."
;;   (interactive "P")
;;   (let ((org-refile-target-verify-function))
;;     (org-refile goto default-buffer rfloc msg)))

;; (defun sanityinc/org-agenda-refile-anywhere (&optional goto rfloc no-update)
;;   "A version of `org-agenda-refile' which allows refiling to any subtree."
;;   (interactive "P")
;;   (let ((org-refile-target-verify-function))
;;     (org-agenda-refile goto rfloc no-update)))

;; ;; Targets start with the file name - allows creating level 1 tasks
;; ;;(setq org-refile-use-outline-path (quote file))
;; (setq org-refile-use-outline-path t)
;; (setq org-outline-path-complete-in-steps nil)

;; ;; Allow refile to create parent tasks with confirmation
;; (setq org-refile-allow-creating-parent-nodes 'confirm)

;; 
;; ;;; To-do settings

;; (setq org-todo-keywords
;;       (quote ((sequence "TODO(t)" "NEXT(n)" "|" "DONE(d!/!)")
;;               (sequence "PROJECT(p)" "|" "DONE(d!/!)" "CANCELLED(c@/!)")
;;               (sequence "WAITING(w@/!)" "DELEGATED(e!)" "HOLD(h)" "|" "CANCELLED(c@/!)")))
;;       org-todo-repeat-to-state "NEXT")

;; (setq org-todo-keyword-faces
;;       (quote (("NEXT" :inherit warning)
;;               ("PROJECT" :inherit font-lock-string-face))))
(setq org-todo-keywords
      (quote ((sequence "TODO(t)" "NEXT(n)" "|" "DONE(d)")
              (sequence "WAITING(w@/!)" "HOLD(h@/!)" "|" "CANCELLED(c@/!)" "PHONE" "MEETING"))))

(setq org-todo-keyword-faces
      (quote (("TODO" :foreground "red" :weight bold)
              ("NEXT" :foreground "blue" :weight bold)
              ("DONE" :foreground "forest green" :weight bold)
              ("WAITING" :foreground "orange" :weight bold)
              ("HOLD" :foreground "magenta" :weight bold)
              ("CANCELLED" :foreground "forest green" :weight bold)
              ("MEETING" :foreground "forest green" :weight bold)
              ("PHONE" :foreground "forest green" :weight bold))))

(setq org-todo-state-tags-triggers
      (quote (("CANCELLED" ("CANCELLED" . t))
              ("WAITING" ("WAITING" . t))
              ("HOLD" ("WAITING") ("HOLD" . t))
              (done ("WAITING") ("HOLD"))
              ("TODO" ("WAITING") ("CANCELLED") ("HOLD"))
              ("NEXT" ("WAITING") ("CANCELLED") ("HOLD"))
              ("DONE" ("WAITING") ("CANCELLED") ("HOLD")))))

					; Tags with fast selection keys
(setq org-tag-alist (quote ((:startgroup)
                            ("@office" . ?o)
                            ("@home" . ?H)
                            (:endgroup)
                            ("WAITING" . ?w)
                            ("HOLD" . ?h)
                            ("PERSONAL" . ?P)
                            ("WORK" . ?W)
                            ("FOLIATIONS" . ?F)
                            ("NOTE" . ?n)
                            ("CANCELLED" . ?c)
                            ("FLAGGED" . ??))))

;; Allow setting single tags without the menu
(setq org-fast-tag-selection-single-key t)

;; For tag searches ignore tasks with scheduled and deadline dates
(setq org-agenda-tags-todo-honor-ignore-options t)

;;; Agenda views

;; (setq-default org-agenda-clockreport-parameter-plist '(:link t :maxlevel 3))

;; (let ((active-project-match "-INBOX/PROJECT"))

;;   (setq org-stuck-projects
;;         `(,active-project-match ("NEXT")))

;;   (setq org-agenda-compact-blocks t
;;         org-agenda-sticky t
;;         org-agenda-start-on-weekday nil
;;         org-agenda-span 'day
;;         org-agenda-include-diary nil
;;         org-agenda-sorting-strategy
;;         '((agenda habit-down time-up user-defined-up effort-up category-keep)
;;           (todo category-up effort-up)
;;           (tags category-up effort-up)
;;           (search category-up))
;;         org-agenda-window-setup 'current-window
;;         org-agenda-custom-commands
;;         `(("N" "Notes" tags "NOTE"
;;            ((org-agenda-overriding-header "Notes")
;;             (org-tags-match-list-sublevels t)))
;;           ("g" "GTD"
;;            ((agenda "" nil)
;;             (tags "INBOX"
;;                   ((org-agenda-overriding-header "Inbox")
;;                    (org-tags-match-list-sublevels nil)))
;;             (stuck ""
;;                    ((org-agenda-overriding-header "Stuck Projects")
;;                     (org-agenda-tags-todo-honor-ignore-options t)
;;                     (org-tags-match-list-sublevels t)
;;                     (org-agenda-todo-ignore-scheduled 'future)))
;;             (tags-todo "-INBOX"
;;                        ((org-agenda-overriding-header "Next Actions")
;;                         (org-agenda-tags-todo-honor-ignore-options t)
;;                         (org-agenda-todo-ignore-scheduled 'future)
;;                         (org-agenda-skip-function
;;                          '(lambda ()
;;                             (or (org-agenda-skip-subtree-if 'todo '("HOLD" "WAITING"))
;;                                 (org-agenda-skip-entry-if 'nottodo '("NEXT")))))
;;                         (org-tags-match-list-sublevels t)
;;                         (org-agenda-sorting-strategy
;;                          '(todo-state-down effort-up category-keep))))
;;             (tags-todo ,active-project-match
;;                        ((org-agenda-overriding-header "Projects")
;;                         (org-tags-match-list-sublevels t)
;;                         (org-agenda-sorting-strategy
;;                          '(category-keep))))
;;             (tags-todo "-INBOX/-NEXT"
;;                        ((org-agenda-overriding-header "Orphaned Tasks")
;;                         (org-agenda-tags-todo-honor-ignore-options t)
;;                         (org-agenda-todo-ignore-scheduled 'future)
;;                         (org-agenda-skip-function
;;                          '(lambda ()
;;                             (or (org-agenda-skip-subtree-if 'todo '("PROJECT" "HOLD" "WAITING" "DELEGATED"))
;;                                 (org-agenda-skip-subtree-if 'nottododo '("TODO")))))
;;                         (org-tags-match-list-sublevels t)
;;                         (org-agenda-sorting-strategy
;;                          '(category-keep))))
;;             (tags-todo "/WAITING"
;;                        ((org-agenda-overriding-header "Waiting")
;;                         (org-agenda-tags-todo-honor-ignore-options t)
;;                         (org-agenda-todo-ignore-scheduled 'future)
;;                         (org-agenda-sorting-strategy
;;                          '(category-keep))))
;;             (tags-todo "/DELEGATED"
;;                        ((org-agenda-overriding-header "Delegated")
;;                         (org-agenda-tags-todo-honor-ignore-options t)
;;                         (org-agenda-todo-ignore-scheduled 'future)
;;                         (org-agenda-sorting-strategy
;;                          '(category-keep))))
;;             (tags-todo "-INBOX"
;;                        ((org-agenda-overriding-header "On Hold")
;;                         (org-agenda-skip-function
;;                          '(lambda ()
;;                             (or (org-agenda-skip-subtree-if 'todo '("WAITING"))
;;                                 (org-agenda-skip-entry-if 'nottodo '("HOLD")))))
;;                         (org-tags-match-list-sublevels nil)
;;                         (org-agenda-sorting-strategy
;;                          '(category-keep))))
;;             ;; (tags-todo "-NEXT"
;;             ;;            ((org-agenda-overriding-header "All other TODOs")
;;             ;;             (org-match-list-sublevels t)))
;;             )))))


;; (add-hook 'org-agenda-mode-hook 'hl-line-mode)

(setq calendar-week-start-day 1)

(defvar bh/hide-scheduled-and-waiting-next-tasks t)
;; Custom agenda command definitions
(setq org-agenda-custom-commands
      (quote (("N" "Notes" tags "NOTE"
               ((org-agenda-overriding-header "Notes")
                (org-tags-match-list-sublevels t)))
              ("h" "Habits" tags-todo "STYLE=\"habit\""
               ((org-agenda-overriding-header "Habits")
                (org-agenda-sorting-strategy
                 '(todo-state-down effort-up category-keep))))
              ("A" "Agenda"
               ((agenda "" nil)
                (tags "REFILE"
                      ((org-agenda-overriding-header "Tasks to Refile")
                       (org-tags-match-list-sublevels nil)))
                (tags-todo "-CANCELLED/!"
                           ((org-agenda-overriding-header "Stuck Projects")
                            (org-agenda-skip-function 'bh/skip-non-stuck-projects)
                            (org-agenda-sorting-strategy
                             '(category-keep))))
                (tags-todo "-HOLD-CANCELLED/!"
                           ((org-agenda-overriding-header "Projects")
                            (org-agenda-skip-function 'bh/skip-non-projects)
                            (org-tags-match-list-sublevels 'indented)
                            (org-agenda-sorting-strategy
                             '(category-keep))))
                (tags-todo "-CANCELLED/!NEXT"
                           ((org-agenda-overriding-header (concat "Project Next Tasks"
                                                                  (if bh/hide-scheduled-and-waiting-next-tasks
                                                                      ""
                                                                    " (including WAITING and SCHEDULED tasks)")))
                            (org-agenda-skip-function 'bh/skip-projects-and-habits-and-single-tasks)
                            (org-tags-match-list-sublevels t)
                            (org-agenda-todo-ignore-scheduled bh/hide-scheduled-and-waiting-next-tasks)
                            (org-agenda-todo-ignore-deadlines bh/hide-scheduled-and-waiting-next-tasks)
                            (org-agenda-todo-ignore-with-date bh/hide-scheduled-and-waiting-next-tasks)
                            (org-agenda-sorting-strategy
                             '(todo-state-down effort-up category-keep))))
                (tags-todo "-REFILE-CANCELLED-WAITING-HOLD/!"
                           ((org-agenda-overriding-header (concat "Project Subtasks"
                                                                  (if bh/hide-scheduled-and-waiting-next-tasks
                                                                      ""
                                                                    " (including WAITING and SCHEDULED tasks)")))
                            (org-agenda-skip-function 'bh/skip-non-project-tasks)
                            (org-agenda-todo-ignore-scheduled bh/hide-scheduled-and-waiting-next-tasks)
                            (org-agenda-todo-ignore-deadlines bh/hide-scheduled-and-waiting-next-tasks)
                            (org-agenda-todo-ignore-with-date bh/hide-scheduled-and-waiting-next-tasks)
                            (org-agenda-sorting-strategy
                             '(category-keep))))
                (tags-todo "-REFILE-CANCELLED-WAITING-HOLD/!"
                           ((org-agenda-overriding-header (concat "Standalone Tasks"
                                                                  (if bh/hide-scheduled-and-waiting-next-tasks
                                                                      ""
                                                                    " (including WAITING and SCHEDULED tasks)")))
                            (org-agenda-skip-function 'bh/skip-project-tasks)
                            (org-agenda-todo-ignore-scheduled bh/hide-scheduled-and-waiting-next-tasks)
                            (org-agenda-todo-ignore-deadlines bh/hide-scheduled-and-waiting-next-tasks)
                            (org-agenda-todo-ignore-with-date bh/hide-scheduled-and-waiting-next-tasks)
                            (org-agenda-sorting-strategy
                             '(category-keep))))
                (tags-todo "-CANCELLED+WAITING|HOLD/!"
                           ((org-agenda-overriding-header (concat "Waiting and Postponed Tasks"
                                                                  (if bh/hide-scheduled-and-waiting-next-tasks
                                                                      ""
                                                                    " (including WAITING and SCHEDULED tasks)")))
                            (org-agenda-skip-function 'bh/skip-non-tasks)
                            (org-tags-match-list-sublevels nil)
                            (org-agenda-todo-ignore-scheduled bh/hide-scheduled-and-waiting-next-tasks)
                            (org-agenda-todo-ignore-deadlines bh/hide-scheduled-and-waiting-next-tasks)))
                (tags "-REFILE/"
                      ((org-agenda-overriding-header "Tasks to Archive")
                       (org-agenda-skip-function 'bh/skip-non-archivable-tasks)
                       (org-tags-match-list-sublevels nil))))
               nil))))


;;; Org clock

;; Show lot of clocking history so it's easy to pick items off the C-F11 list
(setq org-clock-history-length 23)
;; Resume clocking task on clock-in if the clock is open
(setq org-clock-in-resume t)
;; Change tasks to NEXT when clocking in
(setq org-clock-in-switch-to-state 'fred-clock-in-to-next)
;; Separate drawers for clocking and logs
(setq org-drawers (quote ("PROPERTIES" "LOGBOOK")))
;; Save clock data and state changes and notes in the LOGBOOK drawer
(setq org-clock-into-drawer t)
;; Sometimes I change tasks I'm clocking quickly - this removes clocked tasks with 0:00 duration
(setq org-clock-out-remove-zero-time-clocks t)
;; Clock out when moving task to a done state
(setq org-clock-out-when-done t)
;; Save the running clock and all clock history when exiting Emacs, load it on startup
(setq org-clock-persist t)
;; Do not prompt to resume an active clock
(setq org-clock-persist-query-resume nil)
;; Enable auto clock resolution for finding open clocks
(setq org-clock-auto-clock-resolution (quote when-no-clock-is-running))
;; Include current clocking task in clock reports
(setq org-clock-report-include-clocking-task t)

(defun fred-clock-in-to-next (kw)
  "Switch a task from TODO to NEXT when clocking in.
Skips capture tasks, projects, and subprojcets.
Switch projcets and subprojects from NEXT back to TODO"
  (when (not (and (boundp 'org-capture-mode) org-capture-mode))
    (cond ((and (member (org-get-todo-state) (list "TODO"))
		(fred-is-task-p))
	   "NEXT")
	  ((and (member (org-get-todo-state) (list "NEXT"))
		(fred-is-project-p))
	   "TODO"))))

;;; Show the clocked-in task - if any - in the header line
;; (defun sanityinc/show-org-clock-in-header-line ()
;;   (setq-default header-line-format '((" " org-mode-line-string " "))))

;; (defun sanityinc/hide-org-clock-from-header-line ()
;;   (setq-default header-line-format nil))

;; (add-hook 'org-clock-in-hook 'sanityinc/show-org-clock-in-header-line)
;; (add-hook 'org-clock-out-hook 'sanityinc/hide-org-clock-from-header-line)
;; (add-hook 'org-clock-cancel-hook 'sanityinc/hide-org-clock-from-header-line)

;; (after-load 'org-clock
;;   (define-key org-clock-mode-line-map [header-line mouse-2] 'org-clock-goto)
;;   (define-key org-clock-mode-line-map [header-line mouse-1] 'org-clock-menu))



;;; Archiving

;; ;; (setq org-archive-mark-done nil)
;; ;; (setq org-archive-location "%s_archive::* Archive")



;;; Helper functions
(defmacro make-function (name-and-func)
  `(defun ,(intern (format "fred-is-%s-p" (car name-and-func))) ()
     (save-restriction
       (widen)
       (let ((has-subtask nil)
	     (subtree-end (save-excursion (org-end-of-subtree)))
	     (is-a-task (member (nth 2 (org-heading-components)) org-todo-keywords-1)))
	 (save-excursion
	   (forward-line 1)
	   (while (and (not has-subtask)
		       (< (point) subtree-end)
		       (re-search-forward "^\*+ " subtree-end t) )  ;next header
	     (when (member (org-get-todo-state) org-todo-keywords-1)
	       (setq has-subtask t))))
	 (and is-a-task (,(cdr name-and-func) has-subtask))))))

(make-function ("task" . not))
(make-function ("project" . identity))



;; ;; (require-package 'org-pomodoro)
;; ;; (setq org-pomodoro-keep-killed-pomodoro-time t)
;; ;; (after-load 'org-agenda
;; ;;   (define-key org-agenda-mode-map (kbd "P") 'org-pomodoro))


;; ;; ;; Show iCal calendars in the org agenda
;; ;; (when (and *is-a-mac* (require 'org-mac-iCal nil t))
;; ;;   (setq org-agenda-include-diary t
;; ;;         org-agenda-custom-commands
;; ;;         '(("I" "Import diary from iCal" agenda ""
;; ;;            ((org-agenda-mode-hook #'org-mac-iCal)))))

;; ;;   (add-hook 'org-agenda-cleanup-fancy-diary-hook
;; ;;             (lambda ()
;; ;;               (goto-char (point-min))
;; ;;               (save-excursion
;; ;;                 (while (re-search-forward "^[a-z]" nil t)
;; ;;                   (goto-char (match-beginning 0))
;; ;;                   (insert "0:00-24:00 ")))
;; ;;               (while (re-search-forward "^ [a-z]" nil t)
;; ;;                 (goto-char (match-beginning 0))
;; ;;                 (save-excursion
;; ;;                   (re-search-backward "^[0-9]+:[0-9]+-[0-9]+:[0-9]+ " nil t))
;; ;;                 (insert (match-string 0))))))


(after-load 'org
  (define-key org-mode-map (kbd "C-M-<up>") 'org-up-element)
  (define-key org-mode-map (kbd "C-x C-l") 'org-toggle-link-display)
  (define-key org-mode-map (kbd "C-c x") 'org-todo)
  (when *is-a-mac*
    (define-key org-mode-map (kbd "M-h") nil)))

(after-load 'org
  (org-babel-do-load-languages
   'org-babel-load-languages
   `((R . t)
     (ditaa . t)
     (dot . t)
     (emacs-lisp . t)
     (lisp . t)
     (gnuplot . t)
     (haskell . nil)
     (latex . t)
     (ledger . t)
     (ocaml . nil)
     (octave . t)
     (plantuml . t)
     (python . t)
     (ruby . t)
     (screen . nil)
     (,(if (locate-library "ob-sh") 'sh 'shell) . t)
     (sql . nil)
     (sqlite . t))))

(provide 'init-org)
;;; init-org.el ends here
