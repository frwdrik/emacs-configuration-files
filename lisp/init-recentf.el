;;; init-recentf.el --- Settings for tracking recent files -*- lexical-binding: t -*-
;;; Commentary:
;;; Code:

(use-package recentf
  :bind ("C-x C-r" . recentf-open-files)
  :config
  (recentf-mode 1)
  (setq-default recentf-max-saved-items 1000
		recentf-exclude '("/tmp/"))
  (setq save-silently t)
  (run-at-time nil (* 3 60) 'recentf-save-list)
  (add-to-list 'recentf-exclude "emacs/recentf\\'")
  ;; Elisp regex trips me up. But the basic rule is to use twice as
  ;; many backslashes as what you want. For instance, the regex below
  ;; uses two backslashes to escape the stop, so that it matches a
  ;; literal stop instead of any character. The reason for this seems
  ;; to be that elisp reads strings in the exact same way it prints
  ;; them, and it prints as single backslash as '\\'. Thus to produce
  ;; a string with a single backslash, we must type in '\\'.
  ;; Alternatively, we could use (rx ".bookmarks.el" eol) to generate the regex.
  (add-to-list 'recentf-exclude "\\.bookmarks.el\\'"))


(provide 'init-recentf)
;;; init-recentf.el ends here
