;;; init-latex.el --- Provide LaTeX functions  -*- lexical-binding: t -*-
;;; Commentary:
;;; Code:

(use-package tex
  :ensure auctex
  :after org-roam
  :mode ( "\\.tex\\'" . tex-mode)
  :config
  (setq TeX-clean-confirm nil)
  (use-package company-auctex
    :ensure t)
  ;; (use-package company
  ;;   :ensure t
  ;;   :defer t)

  ;; Turn on RefTeX and company-auctex in AUCTeX
  (add-hook 'LaTeX-mode-hook 'turn-on-reftex)

  ;; Activate nice interface between RefTeX and AUCTeX
  (setq reftex-plug-into-AUCTeX t)
  (setq reftex-default-bibliography
	(file-truename "~/imperial/bibliography.bib"))
  (setq TeX-parse-self t)
  (setq TeX-auto-save t)

  ;; Set bibliography for bibtex-completion. Use with ivy-bibtex.
  (setq bibtex-completion-bibliography
	(file-truename "~/imperial/bibliography.bib"))
  (setq bibtex-completion-library-path
	"/home/frwdrik/Documents/bibtex")

  ;; Save notes for bib entries in this directory
  (setq bibtex-completion-notes-path org-roam-directory)
  ;; Open PDF's externally instead of in Emacs
  (setq bibtex-completion-pdf-open-function 'counsel-locate-action-extern)

  ;; Make LaTeX and RefTeX aware of math environments
  (setq reftex-label-alist
	'(
	  ("definition" ?d "def:" "~\\cref{%s}" t  ("definition"))
	  ("theorem" ?t "thm:" "~\\cref{%s}" t  ("theorem" "th."))
	  ("proof" ?p "proof:" "~\\cref{%s}" t  ("proof" "pf."))
	  ("proposition" ?P "prop:" "~\\cref{%s}" t  ("proposition" "prop."))
	  ("lemma" ?l "lem:" "~\\cref{%s}" t  ("lemma"))
	  ("corollary" ?c "cor:" "~\\cref{%s}" t  ("corollary" "cor."))
	  ("example" ?x "example:" "~\\cref{%s}" t  ("example" "ex."))
	  ("remark" ?r "remark:" "~\\cref{%s}" t  ("remark" "rmk."))))

  ;; (defcustom LaTeX-theorem-label "thm:"
  ;;   "*Default prefix to theorem labels."
  ;;   :group 'LaTeX-label
  ;;   :group 'LaTeX-environment
  ;;   :type 'string)
  ;; (add-to-list 'LaTeX-label-alist
  ;;	     '("theorem" . LaTeX-theorem-label))

  ;; For helping AucTeX
  (setq TeX-save-query nil)
  (setq TeX-PDF-mode t)
  (setq-default TeX-master nil)

  (defun my-latex-mode-config ()
    "For use in `latex-mode-hook'."
    (local-set-key (kbd "<f9>") 'insert-subscript) ; add a key
					;  (local-set-key (kbd "C-c )") 'reftex-cleveref-cref)
    (setq auto-complete-mode t)
    (display-line-numbers-mode 1)
    (TeX-add-symbols "text")
    (LaTeX-math-mode)
    (auto-fill-mode)
    (setq TeX-view-program-selection
	  (quote
	   (((output-dvi style-pstricks) "dvips and gv")
	    (output-dvi "xdvi")
	    (output-pdf "Zathura")	; [2]
	    (output-html "xdg-open"))))
    (TeX-source-correlate-mode t)
    (setq TeX-source-correlate-start-server t)
    ;; (setq reftex-label-alist '(AMSTeX))	; Remove () around refs
    (add-to-list 'company-backends 'company-yasnippet)
    (company-auctex-init)
    (yas-global-mode 1))

  ;; add to hook
  (add-hook 'LaTeX-mode-hook 'my-latex-mode-config)

  ;; Find TeX on Mac
  (when *is-a-mac*
    (setenv "PATH" (concat (getenv "PATH") ":/Library/TeX/texbin/"))
    (setq exec-path (append exec-path '("/Library/TeX/texbin/"))))

  ;;; Org-export sattings

  ;; Add the memoir class known to org export
  (push '("memoir"
	  "\\documentclass[11pt]{memoir}
[NO-DEFAULT-PACKAGES]"
	  ("\\chapter{%s}" . "\\chapter*{%s}")
	  ("\\section{%s}" . "\\section*{%s}")
	  ("\\subsection{%s}" . "\\subsection*{%s}")
	  ("\\subsubsection{%s}" . "\\subsubsection*{%s}")
	  ("\\paragraph{%s}" . "\\paragraph*{%s}")
	  ("\\subparagraph{%s}" . "\\subparagraph*{%s}"))
	org-latex-classes)

  ;; Star removes self-reference to ToC in ToC
  (setq org-latex-toc-command "\\tableofcontents*\n\n")

  ;; Use biber to process bibliography. biblatex is the Latex package, while biber and bibtex are possible backends.
  (setq org-latex-bib-compiler "biber"))


(provide 'init-latex)
;;; init-latex.el ends here
