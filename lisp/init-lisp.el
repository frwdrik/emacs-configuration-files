;;; init-lisp.el --- Emacs lisp settings, and common config for other lisps -*- lexical-binding: t -*-
;;; Commentary:
;;; Code:

(use-package elisp-slime-nav
  :ensure t
  :hook ((emacs-lisp . turn-on-elisp-slime-nav-mode)
	 (ielm-mode . turn-on-elisp-slime-nav-mode)))
;;(add-hook 'emacs-lisp-mode-hook (lambda () (setq mode-name "ELisp")))


(setq-default initial-scratch-message
              (concat ";; Happy hacking, " user-login-name " - Victoria and Emacs ♥ you!\n\n"))

(diminish 'eldoc-mode)

;;; Company backends
(use-package elisp-mode
  :init
  (defun my-elisp-mode-hook ()
    "Hook for `emacs-lisp-mode'"
    (set (make-local-variable 'company-backends)
         '((company-capf
	    company-dabbrev-code
	    company-yasnippet
	    company-files)))
    (diminish 'elisp-slime-nav-mode))

  (add-hook 'emacs-lisp-mode-hook 'my-elisp-mode-hook)
  (add-hook 'emacs-lisp-mode-hook 'company-mode))


(defun fred-headerise-elisp ()
  "Add minimal header and footer to an elisp buffer in order to placate flycheck."
  (interactive)
  (let ((fname (if (buffer-file-name)
                   (file-name-nondirectory (buffer-file-name))
                 (error "This buffer is not visiting a file"))))
    (save-excursion
      (goto-char (point-min))
      (insert ";;; " fname " --- Insert description here -*- lexical-binding: t -*-\n"
              ";;; Commentary:\n"
              ";;; Code:\n\n")
      (goto-char (point-max))
      (insert ";;; " fname " ends here\n"))))


;; Make C-x C-e run 'eval-region if the region is active

(defun fred-eval-last-sexp-or-region (prefix)
  "Eval region from BEG to END if active, otherwise the last sexp."
  (interactive "P")
  (if (and (mark) (use-region-p))
      (eval-region (min (point) (mark)) (max (point) (mark)))
    (pp-eval-last-sexp prefix)))

(global-set-key [remap eval-expression] 'pp-eval-expression)

(after-load 'lisp-mode
  (define-key emacs-lisp-mode-map (kbd "C-x C-e") 'fred-eval-last-sexp-or-region))

(when (maybe-require-package 'ipretty)
  (add-hook 'after-init-hook 'ipretty-mode))


(defun fred-make-read-only (expression out-buffer-name)
  "Enable `view-mode' in the output buffer - if any - so it can be closed with `\"q\"."
  (when (get-buffer out-buffer-name)
    (with-current-buffer out-buffer-name
      (view-mode 1))))
(advice-add 'pp-display-expression :after 'fred-make-read-only)



(defun fred-maybe-set-bundled-elisp-readonly ()
  "If this elisp appears to be part of Emacs, then disallow editing."
  (when (and (buffer-file-name)
             (string-match-p "\\.el\\.gz\\'" (buffer-file-name)))
    (setq buffer-read-only t)
    (view-mode 1)))

(add-hook 'emacs-lisp-mode-hook 'fred-maybe-set-bundled-elisp-readonly)


;; Use C-c C-z to toggle between elisp files and an ielm session
;; I might generalise this to ruby etc., or even just adopt the repl-toggle package.

(defvar-local fred-repl-original-buffer nil
  "Buffer from which we jumped to this REPL.")

(defvar fred-repl-switch-function 'switch-to-buffer-other-window)

(defun fred-switch-to-ielm ()
  (interactive)
  (let ((orig-buffer (current-buffer)))
    (if (get-buffer "*ielm*")
        (funcall fred-repl-switch-function "*ielm*")
      (ielm))
    (setq fred-repl-original-buffer orig-buffer)))

(defun fred-repl-switch-back ()
  "Switch back to the buffer from which we reached this REPL."
  (interactive)
  (if fred-repl-original-buffer
      (funcall fred-repl-switch-function fred-repl-original-buffer)
    (error "No original buffer")))

(after-load 'elisp-mode
  (define-key emacs-lisp-mode-map (kbd "C-c C-z") 'fred-switch-to-ielm))
(after-load 'ielm
  (define-key ielm-map (kbd "C-c C-z") 'fred-repl-switch-back))

(global-set-key (kbd "C-c C-z") 'fred-switch-to-ielm)
;; ----------------------------------------------------------------------------
;; Hippie-expand
;; ----------------------------------------------------------------------------

(defun set-up-hippie-expand-for-elisp ()
  "Locally set `hippie-expand' completion functions for use with Emacs Lisp."
  (make-local-variable 'hippie-expand-try-functions-list)
  (add-to-list 'hippie-expand-try-functions-list 'try-complete-lisp-symbol t)
  (add-to-list 'hippie-expand-try-functions-list 'try-complete-lisp-symbol-partially t))


;; ----------------------------------------------------------------------------
;; Automatic byte compilation
;; ----------------------------------------------------------------------------
(when (maybe-require-package 'auto-compile)
  (add-hook 'after-init-hook 'auto-compile-on-save-mode)
  (add-hook 'after-init-hook 'auto-compile-on-load-mode))

;; ----------------------------------------------------------------------------
;; Load .el if newer than corresponding .elc
;; ----------------------------------------------------------------------------
(setq load-prefer-newer t)


(use-package immortal-scratch
  :ensure t
  :hook (after-init . immortal-scratch-mode))


;;; Support byte-compilation in a sub-process, as
;;; required by highlight-cl

(defun fred-byte-compile-file-batch (filename)
  "Byte-compile FILENAME in batch mode, ie. a clean sub-process."
  (interactive "fFile to byte-compile in batch mode: ")
  (let ((emacs (car command-line-args)))
    (compile
     (concat
      emacs " "
      (mapconcat
       'shell-quote-argument
       (list "-Q" "-batch" "-f" "batch-byte-compile" filename)
       " ")))))


;; ----------------------------------------------------------------------------
;; Enable desired features for all lisp modes
;; ----------------------------------------------------------------------------
(defun fred-enable-check-parens-on-save ()
  "Run `check-parens' when the current buffer is saved."
  (add-hook 'after-save-hook #'check-parens nil t))

(defvar fred-lispy-modes-hook
  '(enable-paredit-mode
    fred-enable-check-parens-on-save)
  "Hook run in all Lisp modes.")

(use-package aggressive-indent
  :ensure t
  :diminish
  :hook (fred-lispy-modes . aggressive-indent-mode))

(defun fred-lisp-setup ()
  "Enable features useful in any Lisp mode.
This function is in the hook of modes in `fred-lispy-modes'"
  (run-hooks 'fred-lispy-modes-hook)
  (set (make-local-variable 'company-backends)
       '((company-capf
	  company-dabbrev-code ; slows down typing
	  company-files
	  ;; company-dabbrev    ; slows down typing
	  company-yasnippet     ; always grabs prefix
	  ;; company-keywords   ; doesn't help in elisp mode
	  ;; company-etags      ; not that useful
	  ))))

(defun fred-emacs-lisp-setup ()
  "Enable features useful when working with elisp."
  (set-up-hippie-expand-for-elisp)
  (auto-fill-mode -1))

(defconst fred-elispy-modes
  '(emacs-lisp-mode ielm-mode)
  "Major modes relating to elisp.")

(defconst fred-lispy-modes
  (append fred-elispy-modes
          '(lisp-mode inferior-lisp-mode lisp-interaction-mode))
  "All lispy major modes.")

(use-package derived)

(dolist (hook (mapcar #'derived-mode-hook-name fred-lispy-modes))
  (add-hook hook 'fred-lisp-setup))

(dolist (hook (mapcar #'derived-mode-hook-name fred-elispy-modes))
  (add-hook hook 'fred-emacs-lisp-setup))

(when (boundp 'eval-expression-minibuffer-setup-hook)
  (add-hook 'eval-expression-minibuffer-setup-hook #'eldoc-mode))

(add-auto-mode 'emacs-lisp-mode "\\.emacs-project\\'")
(add-auto-mode 'emacs-lisp-mode "archive-contents\\'")

(require-package 'cl-lib-highlight)
(after-load 'lisp-mode
  (cl-lib-highlight-initialize))

;; ----------------------------------------------------------------------------
;; Delete .elc files when reverting the .el from VC or magit
;; ----------------------------------------------------------------------------

;; When .el files are open, we can intercept when they are modified
;; by VC or magit in order to remove .elc files that are likely to
;; be out of sync.

;; This is handy while actively working on elisp files, though
;; obviously it doesn't ensure that unopened files will also have
;; their .elc counterparts removed - VC hooks would be necessary for
;; that.

(defvar fred-vc-reverting nil
  "Whether or not VC or Magit is currently reverting buffers.")

(defun fred-maybe-remove-elc (&rest _)
  "If reverting from VC, delete any .elc file that will now be out of sync."
  (when fred-vc-reverting
    (when (and (eq 'emacs-lisp-mode major-mode)
               buffer-file-name
               (string= "el" (file-name-extension buffer-file-name)))
      (let ((elc (concat buffer-file-name "c")))
        (when (file-exists-p elc)
          (message "Removing out-of-sync elc file %s" (file-name-nondirectory elc))
          (delete-file elc))))))
(advice-add 'revert-buffer :after 'fred-maybe-remove-elc)

(defun fred-reverting (orig &rest args)
  (let ((fred-vc-reverting t))
    (apply orig args)))
(advice-add 'magit-revert-buffers :around 'fred-reverting)
(advice-add 'vc-revert-buffer-internal :around 'fred-reverting)



(require-package 'macrostep)

(after-load 'lisp-mode
  (define-key emacs-lisp-mode-map (kbd "C-c e") 'macrostep-expand))



;; A quick way to jump to the definition of a function given its key binding
(global-set-key (kbd "C-h K") 'find-function-on-key)


;; Extras for theme editing
(use-package rainbow-mode
  :ensure t
  :diminish
  :hook help-mode)
(defun fred-enable-rainbow-mode-if-theme ()
  (when (and (buffer-file-name) (string-match-p "\\(color-theme-\\|-theme\\.el\\)" (buffer-file-name)))
    (rainbow-mode)))
(add-hook 'emacs-lisp-mode-hook 'fred-enable-rainbow-mode-if-theme)
(add-hook 'help-mode-hook 'rainbow-mode)


(use-package highlight-quoted
  :ensure t
  :hook (emacs-lisp-mode . highlight-quoted-mode))


;; (use-package flycheck
;;   :ensure t
;;   :hook (emacs-lisp-mode . flycheck-package-setup))

;; (when (maybe-require-package 'flycheck)
;;   (require-package 'flycheck-package)
;;   (after-load 'flycheck
;;     (after-load 'elisp-mode
;;       (flycheck-package-setup))))


;; ERT
(after-load 'ert
  (define-key ert-results-mode-map (kbd "g") 'ert-results-rerun-all-tests))



(provide 'init-lisp)
;;; init-lisp.el ends here
