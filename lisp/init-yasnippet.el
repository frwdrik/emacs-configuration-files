;;; init-yasnippets.el --- Provide yasnippets functinos  -*- lexical-binding: t -*-
;;; Commentary:
;;; Code:

(use-package yasnippet
  :ensure t
  :hook ((tex-mode . yas-minor-mode-on)
	 (sh-mode . yas-minor-mode-on)
	 (lisp-mode . yas-minor-mode-on))
  :diminish yas-minor-mode
  :config
  (yas-global-mode 1)
  (load (concat user-emacs-directory
		(convert-standard-filename
		 "snippets/yasnippet-snippets/yasnippet-snippets.el")))
  (require 'yasnippet-snippets)
  (setq yas-triggers-in-field t))

(provide 'init-yasnippet)
;;; init-yasnippet.el ends here
