;;; init-whitespace.el --- Special handling for whitespace -*- lexical-binding: t -*-
;;; Commentary:
;;; Code:

(use-package whitespace-cleanup-mode
  :ensure t
  :defer 0.4
  :diminish
  ;; :hook (after-init . global-whitespace-cleanup-mode)
  :config
  (setq-default show-trailing-whitespace nil))


;;; Whitespace

(defun sanityinc/no-trailing-whitespace ()
  "Turn off display of trailing whitespace in this buffer."
  (setq show-trailing-whitespace nil))

;; But don't show trailing whitespace in SQLi, inf-ruby etc.
(dolist (hook '(special-mode-hook
                Info-mode-hook
                eww-mode-hook
                term-mode-hook
                comint-mode-hook
                compilation-mode-hook
                twittering-mode-hook
                minibuffer-setup-hook))
  (add-hook hook #'sanityinc/no-trailing-whitespace))

(require-package 'whitespace-cleanup-mode)
(add-hook 'after-init-hook 'global-whitespace-cleanup-mode)
(after-load 'whitespace-cleanup-mode
  (diminish 'whitespace-cleanup-mode))

(global-set-key [remap just-one-space] 'cycle-spacing)

;;; https://dougie.io/emacs/indentation/
;;; Show tabs indentation as pipes -> |
;; (global-whitespace-mode)
;; (setq whitespace-style '(face tabs tab-mark trailing))
;; (custom-set-faces
;;  '(whitespace-tab ((t (:foreground "#636363")))))

;; (setq whitespace-display-mappings
;;       '((tab-mark 9 [124 9] [92 9])))

(provide 'init-whitespace)
;;; init-whitespace.el ends here
