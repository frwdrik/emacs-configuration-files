;;; init-misc.el --- Miscellaneous config -*- lexical-binding: t -*-
;;; Commentary:
;;; Code:

;;----------------------------------------------------------------------------
;; Misc config - yet to be placed in separate files
;;----------------------------------------------------------------------------
(fset 'yes-or-no-p 'y-or-n-p)

(add-hook 'prog-mode-hook 'goto-address-prog-mode)
(setq goto-address-mail-face 'link)

(add-hook 'after-save-hook 'executable-make-buffer-file-executable-if-script-p)
;; (add-hook 'after-save-hook 'sanityinc/set-mode-for-new-scripts)

;; (defun sanityinc/set-mode-for-new-scripts ()
;;   "Invoke `normal-mode' if this file is a script and in `fundamental-mode'."
;;   (and
;;    (eq major-mode 'fundamental-mode)
;;    (>= (buffer-size) 2)
;;    (save-restriction
;;      (widen)
;;      (string= "#!" (buffer-substring (point-min) (+ 2 (point-min)))))
;;    (normal-mode)))

(global-set-key (kbd "C-x k") 'kill-this-buffer)

(defun fred-switch-to-previous-buffer ()
  "Switch to previously open buffer.
Repeated invocations toggle between the two most recently open buffers."
  (interactive)
  (switch-to-buffer (other-buffer (current-buffer) 1)))
(global-set-key (kbd "C-c b") #'fred-switch-to-previous-buffer)

(defun open-terminal-in-workdir ()
  (interactive)
  (call-process-shell-command
   (concat "konsole --workdir " default-directory) nil 0))

(global-set-key (kbd "C-c t") 'open-terminal-in-workdir)

(use-package info-colors
  :ensure t
  :hook (Info-selection . info-colors-fontify-node))

;; Handle the prompt pattern for the 1password command-line interface
(after-load 'comint
  (setq comint-password-prompt-regexp
        (concat
         comint-password-prompt-regexp
         "\\|^Please enter your password for user .*?:\\s *\\'")))

(use-package regex-tool
  :config
  (setq-default regex-tool-backend 'perl))

(after-load 're-builder
  ;; Support a slightly more idiomatic quit binding in re-builder
  (define-key reb-mode-map (kbd "C-c C-k") 'reb-quit))

(add-auto-mode 'conf-mode "^Procfile\\'")
(add-auto-mode 'tcl-mode "^Portfile\\'")

(when (daemonp)
  (add-hook 'after-make-frame-functions
	    (lambda (frame)
	      (with-selected-frame frame
		(load-file custom-file)))))

(provide 'init-misc)
;;; init-misc.el ends here
