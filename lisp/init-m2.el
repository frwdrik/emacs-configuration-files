;;; init-m2.el --- Provide interface to M2  -*- lexical-binding: t -*-
;;; Commentary:
;;; Code:


;; add "/usr/local/share/emacs/site-lisp" to load-path if it isn't there
(if (not (member "/usr/local/share/emacs/site-lisp" load-path))
    (setq load-path (cons "/usr/local/share/emacs/site-lisp" load-path)))

;; add "/usr/local/share/info" to Info-default-directory-list if it isn't there
(if (not (member "/usr/local/share/info" Info-default-directory-list))
     (setq Info-default-directory-list (cons "/usr/local/share/info" Info-default-directory-list)))

;; add "/usr/local/bin" to PATH if it isn't there
(if (not (string-match "/usr/local/bin" (getenv "PATH")))
    (setenv "PATH" "/usr/local/bin:$PATH" t))

;; this version will give an error if M2-init.el is not found:
;; (load "M2-init")

;; this version will not give an error if M2-init.el is not found:
(use-package M2
  :bind ([f12] . M2))
;(load "M2-init" t)

;; You may comment out the following line with an initial semicolon if you
;; want to use your f12 key for something else.  However, this action
;; will be undone the next time you run setup() or setupEmacs().
;; (global-set-key [ f12 ] 'M2)

(provide 'init-m2)
;;; init-m2.el ends here
