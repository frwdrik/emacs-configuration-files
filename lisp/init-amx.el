;;; init-amx.el --- Use amx to improve M-x -*- lexical-binding: t -*-
;;; Commentary: This file is no longer loaded as the code is placed in init-ivy.el
;;; Code:

;; Use amx to handle M-x
;; Amx is the _command_, and ivy/counsel is the _completion_ framework
(use-package amx
  :ensure t
  :config
  (amx-mode 1)
  (setq amx-backend 'ivy))

(provide 'init-amx)
;;; init-amx.el ends here
