;;; init-popwin.el --- popup window manager -*- lexical-binding: t -*-
;;; Commentary:
;;; Code:

(use-package popwin
  :ensure t
  :config
  (popwin-mode 1)
  (push '("*Completions*" :stick t) popwin:special-display-config)
  (push '("*Pp Eval Output*" :stick t) popwin:special-display-config)
  (push '("*Help*" :stick t) popwin:special-display-config)
  (push '("*TeX Help*" :stick t) popwin:special-display-config)
  (push '("*grep*" :stick t) popwin:special-display-config)
  (push '("*xref*" :stick t) popwin:special-display-config)
  (push '("*compilation*" :stick t) popwin:special-display-config)
  (push '("*slime-macroexpansion*" :stick t) popwin:special-display-config)
  (push '("*Backtrace*" :stick t) popwin:special-display-config)
  (push '(help-mode :stick t) popwin:special-display-config)
  (push '(helpful-mode :stick t) popwin:special-display-config)
  (push ' (ivy-occur-mode :stick t) popwin:special-display-config)
  ;;  (push ' (sldb-mode :stick t) popwin:special-display-config)  ;; This messes up windows
  )
;; (setq popwin:special-display-config nil)


;; Helpful - a better *help* buffer

;;(require 'helpful)
(use-package helpful
  :ensure t
  :after counsel
  :config
  (setq helpful-max-buffers 0
	counsel-describe-function-function #'helpful-callable
	counsel-describe-variable-function #'helpful-variable)
  (define-key global-map (kbd "C-h k") #'helpful-key))

(provide 'init-popwin)
;;; init-popwin.el ends here
