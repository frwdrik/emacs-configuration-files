;;; init-paredit.el --- Configure paredit structured editing -*- lexical-binding: t -*-
;;; Commentary:
;;; Code:

(use-package paredit
  :ensure t
  :hook ((emacs-lisp-mode
	  ielm-mode
	  lisp-mode
	  inferior-lisp-mode
	  lisp-interaction-mode)
	 . paredit-mode)
  :diminish
  :config
  (fred-after-load-paredit)
  )

(defun maybe-map-paredit-newline ()
  (unless (or (memq major-mode '(inferior-emacs-lisp-mode cider-repl-mode))
              (minibufferp))
    (local-set-key (kbd "RET")  'paredit-newline)))

(defun fred-after-load-paredit ()
  (maybe-map-paredit-newline)
  (dolist (binding '("C-<left>" "C-<right>" "C-M-<left>" "C-M-<right>" "M-s" "M-?"))
    (define-key paredit-mode-map (read-kbd-macro binding) nil))

  ;; Compatibility with other modes
  (sanityinc/suspend-mode-during-cua-rect-selection 'paredit-mode)

  ;; Use paredit in the minibuffer
  ;; TODO: break out into separate package
  ;; http://emacsredux.com/blog/2013/04/18/evaluate-emacs-lisp-in-the-minibuffer/
  (add-hook 'minibuffer-setup-hook 'conditionally-enable-paredit-mode)

  (defvar paredit-minibuffer-commands '(eval-expression
					pp-eval-expression
					eval-expression-with-eldoc
					ibuffer-do-eval
					ibuffer-do-view-and-eval)
    "Interactive commands for which paredit should be enabled in the minibuffer.")

  ;; Don't insert space before delimiters that come after certain symbols
  (defun fred-paredit-space-for-delimiter-after-at (endp delimiter)
    (not (memq (if endp (char-after) (char-before))
	       (list ?@))))
  (add-to-list 'paredit-space-for-delimiter-predicates 'fred-paredit-space-for-delimiter-after-at)
  )					

(defun conditionally-enable-paredit-mode ()
  "Enable paredit during lisp-related minibuffer commands."
  (if (memq this-command paredit-minibuffer-commands)
      (enable-paredit-mode)))

;; ----------------------------------------------------------------------------
;; Enable some handy paredit functions in all prog modes
;; ----------------------------------------------------------------------------


;; (require-package 'paredit-everywhere) - using :ensure instead
(use-package paredit-everywhere
  :ensure t
  :diminish
  :bind (("M-s" . nil)
	 ("M-;" . comment-dwim)) ;; Disable paredit-comment-dwim
  :hook (prog-mode . paredit-everywhere-mode)
  :config
  ;;  (global-set-key (kbd "M-;") 'comment-dwim)
  )


(provide 'init-paredit)
;;; init-paredit.el ends here

