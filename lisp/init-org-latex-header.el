(defconst old-org-latex-header
  "\\documentclass{article}
\\usepackage[usenames]{color}
\[PACKAGES]
\[DEFAULT-PACKAGES]
\\pagestyle{empty}             % do not remove
% The settings below are copied from fullpage.sty
\\setlength{\\textwidth}{\\paperwidth}
\\addtolength{\\textwidth}{-3cm}
\\setlength{\\oddsidemargin}{1.5cm}
\\addtolength{\\oddsidemargin}{-2.54cm}
\\setlength{\\evensidemargin}{\\oddsidemargin}
\\setlength{\\textheight}{\\paperheight}
\\addtolength{\\textheight}{-\\headheight}
\\addtolength{\\textheight}{-\\headsep}
\\addtolength{\\textheight}{-\\footskip}
\\addtolength{\\textheight}{-3cm}
\\setlength{\\topmargin}{1.5cm}
\\addtolength{\\topmargin}{-2.54cm}")

(setq extra-org-latex-header "
%% Tikz and some libraries
\\usepackage{tikz}
\\usetikzlibrary{arrows}
\\usetikzlibrary{decorations.markings}
\\usetikzlibrary{decorations.pathmorphing} \\usetikzlibrary{positioning}
\\usetikzlibrary{fadings} \\usetikzlibrary{intersections}
\\usetikzlibrary{cd} \\usetikzlibrary{shadows} \\usetikzlibrary{calc}
\\usetikzlibrary{through}
\\usetikzlibrary{shapes}

%% Matrix groups
\\newcommand{\\GL}{\\mathrm{GL}}
\\newcommand{\\Or}{\\mathrm{O}}
\\newcommand{\\PGL}{\\mathrm{PGL}}
\\newcommand{\\PSL}{\\mathrm{PSL}}
\\newcommand{\\PSO}{\\mathrm{PSO}}
\\newcommand{\\PSU}{\\mathrm{PSU}}
\\newcommand{\\SL}{\\mathrm{SL}}
\\newcommand{\\SO}{\\mathrm{SO}}
\\newcommand{\\Spin}{\\mathrm{Spin}}
\\newcommand{\\Sp}{\\mathrm{Sp}}
\\newcommand{\\SU}{\\mathrm{SU}}
\\newcommand{\\U}{\\mathrm{U}}
\\newcommand{\\Mat}{\\mathrm{Mat}}

%% Matrix algebras
\\newcommand{\\lie}[1]{\\mathfrak{#1}}
\\newcommand{\\gl}{\\lie{gl}}
\\newcommand{\\ort}{\\lie{o}}
\\newcommand{\\so}{\\lie{so}}
\\newcommand{\\su}{\\lie{su}}
\\newcommand{\\uu}{\\lie{u}}
\\renewcommand{\\sl}{\\lie{sl}}

\\newcommand{\\puis}[1]{\\langle\\langle{#1}\\rangle\\rangle}

%% Special sets
\\newcommand{\\A}{\\mathbb{A}}
\\newcommand{\\C}{\\mathbb{C}}
\\newcommand{\\CP}{\\mathbb{CP}}
\\newcommand{\\DD}{\\mathbb{D}}
\\newcommand{\\F}{\\mathbb{F}}
\\newcommand{\\GG}{\\mathbb{G}}
\\renewcommand{\\H}{\\mathbb{H}}
\\newcommand{\\N}{\\mathbb{N}}
\\renewcommand{\\P}{\\mathbb{P}}
\\newcommand{\\Q}{\\mathbb{Q}}
\\newcommand{\\R}{\\mathbb{R}}
\\newcommand{\\RP}{\\mathbb{RP}}
\\newcommand{\\T}{\\mathbb{T}}
\\newcommand{\\Z}{\\mathbb{Z}}

%% Vectors
\\renewcommand{\\vec}[1]{\\boldsymbol{\\mathbf{#1}}}

%% Operators
\\let\\Im\\relax \\let\\Re\\relax

\\DeclareMathOperator{\\adj}{adj}
\\DeclareMathOperator{\\Ann}{Ann}
\\DeclareMathOperator{\\Aut}{Aut}
\\DeclareMathOperator{\\Bilin}{Bilin}
\\DeclareMathOperator{\\Bs}{Bs}
\\DeclareMathOperator{\\card}{card}
\\DeclareMathOperator{\\Char}{char}
\\DeclareMathOperator{\\CS}{CS}
\\DeclareMathOperator{\\coker}{coker}
\\DeclareMathOperator{\\conv}{conv}
\\DeclareMathOperator{\\cont}{cont}
\\DeclareMathOperator{\\diag}{diag}
\\DeclareMathOperator{\\diam}{diam}
\\DeclareMathOperator{\\Diff}{Diff}
\\DeclareMathOperator{\\disc}{disc}
\\DeclareMathOperator{\\End}{End}
\\DeclareMathOperator*{\\esssup}{ess\\,sup}
\\DeclareMathOperator{\\ev}{ev}
\\DeclareMathOperator{\\Exc}{Exc}
\\DeclareMathOperator{\\Ext}{Ext}
\\DeclareMathOperator{\\Frac}{Frac}
\\DeclareMathOperator{\\Frob}{Frob}
\\DeclareMathOperator{\\Gal}{Gal}
\\DeclareMathOperator{\\gr}{gr}
\\DeclareMathOperator{\\hcf}{hcf}
\\DeclareMathOperator{\\Hom}{Hom}
\\DeclareMathOperator{\\ht}{ht}
\\DeclareMathOperator{\\id}{id}
\\DeclareMathOperator{\\Im}{Im}
\\DeclareMathOperator{\\im}{im}
\\DeclareMathOperator{\\image}{image}
\\DeclareMathOperator{\\Ind}{Ind}
\\DeclareMathOperator{\\Int}{Int}
\\DeclareMathOperator{\\Isom}{Isom}
\\DeclareMathOperator{\\length}{length}
\\DeclareMathOperator{\\Lie}{Lie}
\\DeclareMathOperator{\\NE}{\\overline{NE}}
\\DeclareMathOperator{\\orb}{orb}
\\DeclareMathOperator{\\ord}{ord}
\\DeclareMathOperator{\\Pic}{Pic}
\\DeclareMathOperator{\\poly}{poly}
\\DeclareMathOperator{\\Proj}{\\mathbf{Proj}}
\\DeclareMathOperator{\\proj}{Proj}
\\DeclareMathOperator{\\rank}{rank}
\\DeclareMathOperator{\\rel}{rel}
\\DeclareMathOperator{\\Rad}{Rad}
\\DeclareMathOperator{\\Re}{Re}
\\DeclareMathOperator*{\\res}{res}
\\DeclareMathOperator{\\Res}{Res}
\\DeclareMathOperator{\\Ric}{Ric}
\\DeclareMathOperator{\\rk}{rk}
\\DeclareMathOperator{\\sgn}{sgn}
\\DeclareMathOperator{\\Spec}{Spec}
\\DeclareMathOperator{\\spn}{span}
\\DeclareMathOperator{\\stab}{stab}
\\DeclareMathOperator{\\St}{St}
\\DeclareMathOperator{\\Supp}{Supp}
\\DeclareMathOperator{\\supp}{supp}
\\DeclareMathOperator{\\Syl}{Syl}
\\DeclareMathOperator{\\Sym}{Sym}
\\DeclareMathOperator{\\Var}{Var}
\\DeclareMathOperator{\\WDiv}{WDiv}
\\DeclareMathOperator{\\vol}{vol}
\\DeclareMathOperator{\\Sing}{Sing}
\\DeclareMathOperator{\\sing}{Sing}
\\DeclareMathOperator{\\tr}{tr}
\\DeclareMathOperator{\\Tr}{Tr}
\\DeclareMathOperator{\\codim}{codim}

% Others
\\newcommand\\ad{\\mathrm{ad}}
\\newcommand\\Art{\\mathrm{Art}}
\\newcommand{\\B}{\\mathcal{B}}
\\newcommand{\\cU}{\\mathcal{U}}
\\newcommand{\\Der}{\\mathrm{Der}}
\\newcommand{\\D}{\\mathrm{D}}
\\newcommand{\\dR}{\\mathrm{dR}}
\\newcommand{\\E}{\\mathcal{E}}
\\newcommand{\\exterior}{\\mathchoice{{\\textstyle\\bigwedge}}{{\\bigwedge}}{{\\textstyle\\wedge}}{{\\scriptstyle\\wedge}}}
\\newcommand{\\cF}{\\mathcal{F}}
\\newcommand{\\G}{\\mathcal{G}}
\\newcommand{\\Gr}{\\mathrm{Gr}}
\\newcommand{\\cH}{\\mathcal{H}}
\\newcommand{\\haut}{\\mathrm{ht}}
\\newcommand{\\Hol}{\\mathrm{Hol}}
\\newcommand{\\hol}{\\mathfrak{hol}}
\\newcommand{\\I}{\\mathcal{I}}
\\newcommand{\\Id}{\\mathrm{Id}}
\\newcommand{\\K}{\\mathcal{K}}
\\newcommand{\\cL}{\\mathcal{L}}
\\newcommand{\\op}{\\mathrm{op}}
\\renewcommand{\\O}{\\mathcal{O}}
\\newcommand{\\pr}{\\mathrm{pr}}
\\newcommand{\\Ps}{\\mathcal{P}}
\\newcommand{\\pt}{\\mathrm{pt}}
\\newcommand{\\qeq}{\\mathrel{``{=}''}}
\\newcommand{\\Rs}{\\mathcal{R}}
\\newcommand{\\Vect}{\\mathrm{Vect}}
\\newcommand{\\wsto}{\\stackrel{\\mathrm{w}^*}{\\to}}
\\newcommand{\\wt}{\\mathrm{wt}}
\\newcommand{\\wto}{\\stackrel{\\mathrm{w}}{\\to}}
\\renewcommand{\\d}{\\mathrm{d}}


%% Custom commands
\\newcommand*{\\Cdot}{{\\raisebox{-0.25ex}{\\scalebox{1.5}{$\\cdot$}}}}
\\newcommand{\\pd}[2][ ]{ \\ifx #1 { } \\frac{\\partial}{\\partial #2} \\else
  \\frac{\\partial{#1}}{\\partial #2} \\fi }

\\newcommand\\restr[2]{{
    \\left.\\kern-\\nulldelimiterspace % automatically resize the bar
				    % with \\right
      #1 % the function
      % \\vphantom{\\big|} % pretend it's a little taller at normal size
  \\right|_{#2} % this is the delimiter
}}


%% Custom TikZ arrow styles
\\pgfarrowsdeclarecombine{twolatex'}{twolatex'}{latex'}{latex'}{latex'}{latex'}
\\tikzset{->/.style = {decoration={markings, mark=at position 1 with
      {\\arrow[scale=2]{latex'}}}, postaction={decorate}}}
\\tikzset{<-/.style = {decoration={markings, mark=at position 0 with
      {\\arrowreversed[scale=2]{latex'}}}, postaction={decorate}}}
\\tikzset{<->/.style = {decoration={markings, mark=at position 0 with
      {\\arrowreversed[scale=2]{latex'}}, mark=at position 1 with
      {\\arrow[scale=2]{latex'}}}, postaction={decorate}}}
\\tikzset{->-/.style = {decoration={markings, mark=at position #1 with
      {\\arrow[scale=2]{latex'}}}, postaction={decorate}}}
\\tikzset{-<-/.style = {decoration={markings, mark=at position #1 with
      {\\arrowreversed[scale=2]{latex'}}}, postaction={decorate}}}
\\tikzset{->>/.style = {decoration={markings, mark=at position 1 with
      {\\arrow[scale=2]{latex'}}}, postaction={decorate}}}
\\tikzset{<<-/.style = {decoration={markings, mark=at position 0 with
      {\\arrowreversed[scale=2]{twolatex'}}}, postaction={decorate}}}
\\tikzset{<<->>/.style = {decoration={markings, mark=at position 0 with
      {\\arrowreversed[scale=2]{twolatex'}}, mark=at position 1 with
      {\\arrow[scale=2]{twolatex'}}}, postaction={decorate}}}
\\tikzset{->>-/.style = {decoration={markings, mark=at position #1 with
      {\\arrow[scale=2]{twolatex'}}}, postaction={decorate}}}
\\tikzset{-<<-/.style = {decoration={markings, mark=at position #1 with
      {\\arrowreversed[scale=2]{twolatex'}}}, postaction={decorate}}}

%% Other TikZ custom styles
\\tikzset{circ/.style = {fill, circle, inner sep = 0, minimum size = 3}}
\\tikzset{scirc/.style = {fill, circle, inner sep = 0, minimum size = 1.5}}
\\tikzset{mstate/.style={circle, draw, blue, text=black, minimum width=0.7cm}}

\\tikzset{eqpic/.style={baseline={([yshift=-.5ex]current bounding box.center)}}}
\\tikzset{commutative diagrams/.cd,cdmap/.style={/tikz/column 1/.append style={anchor=base east},/tikz/column 2/.append style={anchor=base west},row sep=tiny}}

% Define block styles for flowcharts
\\tikzstyle{decision} = [diamond, draw, fill=blue!20,
    text width=6.5em, text badly centered, node distance=3cm, inner sep=2pt]
\\tikzstyle{block} = [rectangle, draw, fill=blue!20,
    text width=5em, text centered, minimum height=4em]
\\tikzstyle{line} = [draw, -latex']
\\tikzstyle{cloud} = [draw, ellipse,fill=red!20, node distance=3cm,
    minimum height=2em]


\\definecolor{mblue}{rgb}{0.2, 0.3, 0.8}
\\definecolor{morange}{rgb}{1, 0.5, 0}
\\definecolor{mgreen}{rgb}{0.1, 0.4, 0.2}
\\definecolor{mred}{rgb}{0.5, 0, 0}

\\def\\drawcirculararc(#1,#2)(#3,#4)(#5,#6){%
    \\pgfmathsetmacro\\cA{(#1*#1+#2*#2-#3*#3-#4*#4)/2}%
    \\pgfmathsetmacro\\cB{(#1*#1+#2*#2-#5*#5-#6*#6)/2}%
    \\pgfmathsetmacro\\cy{(\\cB*(#1-#3)-\\cA*(#1-#5))/%
			((#2-#6)*(#1-#3)-(#2-#4)*(#1-#5))}%
    \\pgfmathsetmacro\\cx{(\\cA-\\cy*(#2-#4))/(#1-#3)}%
    \\pgfmathsetmacro\\cr{sqrt((#1-\\cx)*(#1-\\cx)+(#2-\\cy)*(#2-\\cy))}%
    \\pgfmathsetmacro\\cA{atan2(#2-\\cy,#1-\\cx)}%
    \\pgfmathsetmacro\\cB{atan2(#6-\\cy,#5-\\cx)}%
    \\pgfmathparse{\\cB<\\cA}%
    \\ifnum\\pgfmathresult=1
	\\pgfmathsetmacro\\cB{\\cB+360}%
    \\fi
    \\draw (#1,#2) arc (\\cA:\\cB:\\cr);%
}
")

(provide 'init-org-latex-header)
