;;; init-ivy.el --- Use ivy for minibuffer completion and more -*- lexical-binding: t -*-
;;; Commentary:
;;; Code:


;;; Counsel is in some ways the 'main' package. Loading it causes ivy
;;; to load as well. The setup below defers loading of counsel until
;;; one of the key bindings are pressed.
;;; 


(use-package counsel
  :ensure t
  :diminish
  :bind (("M-x" . counsel-M-x)
	 ("H-f" . counsel-find-file)
	 ("C-h f" . counsel-describe-function)
	 ("C-h v" . counsel-describe-variable)
	 ("C-x b" . ivy-switch-buffer)
	 ("H-b" . ivy-switch-buffer)
	 ("C-x C-r" . counsel-recentf)
	 ("C-x d" . counsel-dired)
	 ("H-L" . counsel-locate)
	 ("H-a" . ag))
  :config
  (setq counsel-mode-override-describe-bindings t
	ivy-initial-inputs-alist nil		 ; overridden if not here
	)

  (defun counsel-locate-function (str)
    (or
     (ivy-more-chars)
     (progn
       (counsel--async-command
	(format "locate %s '%s'"
		;; mapconcat #'identity counsel-locate-options " ")
		""
		(counsel--elisp-to-pcre
		 (ivy--regex str))))
       '("" "working..."))))
  (defun counsel-locate (&optional initial-input)
    "Call the \"locate\" shell command.
INITIAL-INPUT can be given as the initial minibuffer input."
    (interactive)
    (ivy-read "Locate: " #'counsel-locate-function
              :initial-input initial-input
              :dynamic-collection t
              :history 'counsel-locate-history
              :action (lambda (file)
			(with-ivy-window
                          (when file
                            (find-file file))))
              :unwind #'counsel-delete-process
              :caller 'counsel-locate))
  (ivy-mode 1))  ;; has to be here to take effect

(use-package ivy
  :ensure t
  :diminish
  :commands (counsel-find-file)
  ;;  :hook (after-init . ivy-mode)
  :bind (("C-x C-f" . counsel-find-file)
	 :map ivy-minibuffer-map ("<return>" . ivy-alt-done)
	 ("<up>" . ivy-previous-line)
	 ("C-j" . ivy-immediate-done)
	 ("C-<return>" . ivy-immediate-done)
	 ("C-c C-q" . ivy-wgrep-change-to-wgrep-mode))
  :config
  (setq-default ivy-use-virtual-buffers t ; show (closed) recentf files
                ivy-virtual-abbreviate 'fullpath 
                ivy-count-format ""
                projectile-completion-system 'ivy
                ivy-magic-tilde nil
                ivy-dynamic-exhibit-delay-ms 150
		ivy-use-selectable-prompt t
		ivy-on-del-error-function #'ignore)
  (ivy-set-actions t '(("y" (lambda (x) (kill-new x)) "yank")))

  (add-to-list 'ivy-height-alist (cons 'ivy-switch-buffer 14))
  (add-to-list 'ivy-height-alist (cons 'counsel-find-file 14))
  (add-to-list 'ivy-height-alist (cons 'counsel-M-x 12))

  (use-package ivy-hydra
    :ensure t
    :config
    (setq ivy-read-action-function #'ivy-hydra-read-action))
  
  (use-package amx
    :ensure t
    :config
    (amx-mode 1)
    (setq amx-backend 'ivy))
  (use-package ivy-rich
    ;; https://www.reddit.com/r/emacs/comments/910pga/tip_how_to_use_ivy_and_its_utilities_in_your/
    :ensure t
    :config
    (setq ivy-virtual-abbreviate 'full
	  ivy-rich-switch-buffer-align-virtual-buffer nil
	  ivy-rich-parse-remote-buffer nil     ; otherwise it's slow when using tramp
	  ivy-rich-path-style 'abbrev)
    (add-to-list 'ivy--display-transformers-alist
		 (cons 'helpful-callable
		       '(:columns
			 ((counsel-describe-function-transformer (:width 40))
			  (ivy-rich-counsel-function-docstring (:face font-lock-doc-face))))))
    (ivy-rich-mode 1))

  (use-package ivy-bibtex
    :ensure t
    :config
    (setq ivy-bibtex-default-action 'ivy-bibtex-edit-notes)))

;;(global-set-key (kbd "M-x") 'counsel-M-x)

(defun sanityinc/enable-ivy-flx-matching ()
  "Make `ivy' matching work more like IDO."
  (interactive)
  (require-package 'flx)
  (setq-default ivy-re-builders-alist
                '((t . ivy--regex-fuzzy))))

(defun fred-setup-project-search-function ()
  (when (maybe-require-package 'counsel)
    (when (maybe-require-package 'projectile)
      (let ((search-function
             (cond
              ((executable-find "rg") 'counsel-rg)
              ((executable-find "ag") 'counsel-ag)
              ((executable-find "pt") 'counsel-pt)
              ((executable-find "ack") 'counsel-ack))))
	(when search-function
          (defun sanityinc/counsel-search-project (initial-input &optional use-current-dir)
            "Search using `counsel-rg' or similar from the project root for INITIAL-INPUT.
If there is no project root, or if the prefix argument
USE-CURRENT-DIR is set, then search from the current directory
instead."
            (interactive (list (thing-at-point 'symbol)
                               current-prefix-arg))
            (let ((current-prefix-arg)
                  (dir (if use-current-dir
                           default-directory
			 (condition-case err
                             (projectile-project-root)
                           (error default-directory)))))
              (funcall search-function initial-input dir)))))
      (after-load 'ivy
	(add-to-list 'ivy-height-alist (cons 'counsel-ag 20)))
      (global-set-key (kbd "M-?") 'sanityinc/counsel-search-project))))


(when (maybe-require-package 'swiper)
  (after-load 'ivy
    (define-key ivy-mode-map (kbd "M-s /") 'swiper-thing-at-point)))


(when (maybe-require-package 'ivy-xref)
  (setq xref-show-xrefs-function 'ivy-xref-show-xrefs))

(provide 'init-ivy)
;;; init-ivy.el ends here
