;;; init-c.el --- C/C++ editing  -*- lexical-binding: t -*-
;;; Commentary:
;;; Code:

(setq c-default-style "k&r"
      c-basic-offset 4)

;; Package: clean-aindent-mode
;; (require 'clean-aindent-mode)
;; (add-hook 'prog-mode-hook 'clean-aindent-mode)

;; Append to auto-mode-alist for opening xkb config files in c-mode
(dolist (d (list "symbols" "types" "compat"))
  (add-to-list 'auto-mode-alist
	       (cons (format "usr/share/X11/xkb/%s/" d) 'c-mode) t))

(setq path-to-ctags "/usr/bin/ctags")

(defun create-tags (dir-name)
  "Create tags file in path DIR-NAME."
  (interactive "DDirectory: ")
  (shell-command
   (format "%s -f TAGS -e -R %s" path-to-ctags (directory-file-name dir-name))))

(use-package company-c-headers
  :ensure t)
(add-hook 'c-mode-common-hook
	  (lambda ()
	    (when (featurep 'paredit)
	      (define-key paredit-mode-map (kbd "M-;") nil)
	      (local-set-key (kbd "M-;") 'comment-dwim))
	    (yas-global-mode 1)
	    'company-c-headers-setup
	    (setq company-minimum-prefix-length 2)
	    (define-key c-mode-map (kbd "C-<tab>") 'company-complete)
	    (define-key c-mode-map (kbd "<f9>") #'compile)
	    (set (make-local-variable 'company-backends)
		 '((company-c-headers
		    company-clang
					; company-dabbrev-code
		    )
		   (company-dabbrev-code company-gtags company-keywords)
		   company-yasnippet
		   company-dabbrev
		   company-bbdb company-eclim company-semantic  company-cmake company-capf
		   company-files
		   company-oddmuse 
		   ))))

;; from enberg on #emacs
(add-hook 'compilation-finish-functions
	  (lambda (buf str)
	    (if (null (string-match ".*exited abnormally.*" str))
		;;no errors, make the compilation window go away in a few seconds
		(progn
		  (run-at-time
		   "2 sec" nil 'delete-windows-on
		   (get-buffer-create "*compilation*"))
		  (message "No Compilation Errors!")))))


;; global-semantic, a part of CEDET
(require 'semantic)
(global-semantic-highlight-func-mode 1)
(global-semantic-idle-completions-mode 1)
(setf semantic-idle-scheduler-idle-time 0.1)

(provide 'init-c)
;;; init-c.el ends here
