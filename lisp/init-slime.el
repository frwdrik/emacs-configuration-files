;;; init-slime.el --- Slime support for Common Lisp -*- lexical-binding: t -*-
;;; Commentary:
;;; Code:

(require 'xdg)	    ;; for canonical paths

(use-package slime
  :ensure t
  :commands slime
  :diminish (slime-mode . "slime")
  :bind (:map slime-mode-indirect-map
	      ("M-n" . nil)
	      ("M-p" . nil))
  :init
  (setq slime-repl-history-file
	(concat (file-name-as-directory
		 (xdg-cache-home))
		"emacs/slime-history.eld"))
  ;; This needs to be set before slime is loaded, since some contribs
  ;; enable themselves by hooking into slime-mode-hook. Key bindings
  ;; defined by contribs will take precedence in the reverse order
  ;; they are loaded. Thus, unless we load slime-fancy which loads
  ;; slime-autodoc before slime-repl, we should place slime-autodoc
  ;; last, since it has a more useful space key function.
  (setq slime-contribs '(slime-fancy
			 slime-repl
			 slime-repl-ansi-color
			 slime-fuzzy
			 slime-company
					;slime-autodoc
			 ))

  :config
  (yas-minor-mode-on)
  (setq slime-protocol-version 'ignore
	slime-net-coding-system 'utf-8-unix
	slime-complete-symbol*-fancy t
	;; It is important that the program arguments (the strings
	;; following the executable path) contain no whitespace.
	slime-lisp-implementations '((sbcl ("/usr/bin/sbcl"
					    "--noinform"
					    "--userinit"
					    "/home/frwdrik/.config/sbcl/sbclrc")
					   :coding-system utf-8-unix)))

  ;;; CONTRIBS
  ;; Be careful that if the name of a contrib is in `features', then
  ;; slime-setup won't load it. Thus we defer loading of slime-company
  ;; so that slime-setup will actually load it.
  (use-package slime-company
    :ensure t
    :defer t
    :config
    (setq slime-company-completion 'simple)
    ;;(company-quickhelp--enable)
    (setq company-quickhelp-delay 0.2)
    (setq company-minimum-prefix-length 3)

    ;; We redefine slime-company-maybe-enable here.
    (defun slime-company-maybe-enable ()
      (when (slime-company-active-p)
	(company-mode 1)
	(setq company-backends '(company-slime
				 company-files
				 company-dabbrev
				 company-keywords))
	(unless (slime-find-contrib 'slime-fuzzy)
	  (setq slime-company-completion 'simple))))
    (use-package slime-autodoc
      :config
      ;; slime-autodoc sometimes fails because of pending slime-rex-continuations
      (setq slime-inhibit-pipelining nil)))

  (define-key sldb-mode-map (kbd "C-g") 'sldb-quit)

  ;; (setq slime-complete-symbol-function 'slime-fuzzy-complete-symbol)
  (global-set-key [f9] 'slime-selector)

  (defun fred-slime-setup ()
    "Mode setup function for slime lisp buffers."
    (require-package 'hippie-expand-slime)
    (set-up-slime-hippie-expand)
    (auto-fill-mode))
  (add-hook 'slime-mode-hook 'fred-slime-setup)

  ;; Lisp buffers
  (setq inferior-lisp-program "/usr/bin/sbcl --noinform --userinit /home/frwdrik/.config/sbcl/sbclrc"))

;; package.el compiles the contrib subdir, but the compilation order
;; causes problems, so we remove the .elc files there. See
;; http://lists.common-lisp.net/pipermail/slime-devel/2012-February/018470.html
(mapc #'delete-file
      (file-expand-wildcards (concat user-emacs-directory "elpa/slime-2*/contrib/*.elc")))

;;; REPL
(defun fred-slime-repl-setup ()
  "Mode setup function for slime REPL."
  ;; (fred-lisp-setup)
  (enable-paredit-mode)
  ;; (setf company-backends '(company-slime
  ;;			   company-capf
  ;;			   company-dabbrev-code
  ;;			   company-files
  ;;			   company-dabbrev
  ;;			   company-keywords
  ;;			   company-etags))
  (set-up-slime-hippie-expand)
  (setq show-trailing-whitespace nil))

(after-load 'slime-repl
  ;; Stop SLIME's REPL from grabbing DEL, which is annoying when backspacing over a '('
  (after-load 'paredit
    (define-key slime-repl-mode-map (read-kbd-macro paredit-backward-delete-key) nil))

  ;; Bind TAB to `indent-for-tab-command', as in regular Slime buffers.
  (define-key slime-repl-mode-map (kbd "TAB") 'indent-for-tab-command)

  (add-hook 'slime-repl-mode-hook 'fred-slime-repl-setup))

;;; HyperSpec Lookup locally
(setq common-lisp-hyperspec-root
      (concat "file://" (expand-file-name "~/.config/common-lisp/HyperSpec/")))
(setq-local browse-url-browser-function 'eww-browse-url)

(provide 'init-slime)
;;; init-slime.el ends here
