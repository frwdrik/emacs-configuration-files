;;; init-custom-modes.el --- Load custom modes  -*- lexical-binding: t -*-
;;; Commentary:
;;; Code:

(dolist (pattern (list "bspwmrc" "sxhkdrc"))
  (add-to-list 'auto-mode-alist (cons pattern 'bspwm-config-mode)))

(require 'bspwm-config-mode)

(require 'sxhkd-config-mode)

(provide 'init-custom-modes)
;;; init-custom-modes.el ends here
