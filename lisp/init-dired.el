;;; init-dired.el --- Dired customisations -*- lexical-binding: t -*-
;;; Commentary:
;;; Code:

(use-package dired
  :commands counsel-dired
  :bind (:map dired-mode-map
	      ([mouse-2] . dired-find-file)
	      ("C-c C-q" . wdired-change-to-wdired-mode)
	      ("F" . fred-dired-find-files))
  :config
  (setq-default dired-dwim-target t)
  (put 'dired-find-alternate-file 'disabled nil)
  (setq dired-recursive-deletes 'top)
  ;; (define-key dired-mode-map [mouse-2] 'dired-find-file)
  ;; (define-key dired-mode-map (kbd "C-c C-q") 'wdired-change-to-wdired-mode)

  (defun fred-dired-find-files (&optional arg)
    "Open each of the marked files, or the file under the point,
or when prefix arg, the next N files "
    (interactive "P")
    (mapc 'find-file (dired-get-marked-files nil arg)))

  ;; Prefer g-prefixed coreutils version of standard utilities when available
  (let ((gls (executable-find "gls")))
    (when gls (setq insert-directory-program gls)))

  (use-package diredfl
    :ensure t
    :config
    (after-load 'dired
      (diredfl-global-mode)))

  ;; Show human readable file sizes
  (setq dired-listing-switches "-alh")
  (put 'dired-find-alternate-file 'disabled nil)

  (use-package diff-hl
    :ensure t
    :hook (dired-mode . diff-hl-dired-mode)))

(provide 'init-dired)
;;; init-dired.el ends here
