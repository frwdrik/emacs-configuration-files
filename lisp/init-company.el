;;; init-company.el --- Completion with company -*- lexical-binding: t -*-
;;; Commentary:
;;; Code:

;; WAITING: haskell-mode sets tags-table-list globally, breaks tags-completion-at-point-function
;; TODO Default sort order should place [a-z] before punctuation

;; To write a custom backend, check out
;; http://sixty-north.com/blog/writing-the-simplest-emacs-company-mode-backend
(use-package company
  :ensure t
  :diminish
  :hook (after-init . global-company-mode)
  :bind ( ("C-<tab>" . company-indent-or-complete-common)
	  (:map company-active-map
		("C-n" . company-select-next)
		("C-p" . company-select-previous)
		("<tab>" . company-complete-common-or-cycle)
		("<C-tab>" . company-select-previous)
		("M-/" . company-other-backend)
		;;	("<tab>" . company-manual-begin)
		))
  :config
  (dolist (backend '(company-eclim company-semantic))
    (delq backend company-backends))
  (setq company-idle-delay 0.2
	tab-always-indent 'complete
	company-tooltip-limit 12
	company-show-numbers t)		;insert candidate using M-[0-9]   
  
  (setq-default company-dabbrev-other-buffers 'all
                company-tooltip-align-annotations t)
  (add-to-list 'completion-styles 'initials t)

  (use-package company-quickhelp
    :ensure t)
  ;; (use-package company-box
  ;;   :hook (company-mode . company-box-mode)
  ;;   :diminish company-box-mode)
  )


(defun counsel-company ()
  "Complete using `company-candidates'."
  (interactive)
  (unless company-candidates
    (company-complete))
  (when company-point
    (company-complete-common)
    (when (looking-back company-common (line-beginning-position))
      (setq ivy-completion-beg (match-beginning 0))
      (setq ivy-completion-end (match-end 0)))
    (ivy-read "company cand: " (mapcar (lambda (x)
                                         (concat
                                          x
                                          "\t\t"
                                          (company-call-backend 'annotation x)))
                                       company-candidates)
              :action (lambda (x)
                        (ivy-completion-in-region-action
                         (replace-regexp-in-string "\t\t\.*" "" x))
                        (run-at-time 0.01 nil 'company-pseudo-tooltip-hide)))))



;; Suspend page-break-lines-mode while company menu is active
;; (see https://github.com/company-mode/company-mode/issues/416)
(after-load 'company
  (after-load 'page-break-lines
    (defvar-local fred-page-break-lines-on-p nil)

    (defun fred-page-break-lines-disable (&rest ignore)
      (when (setq fred-page-break-lines-on-p (bound-and-true-p page-break-lines-mode))
        (page-break-lines-mode -1)))

    (defun fred-page-break-lines-maybe-reenable (&rest ignore)
      (when fred-page-break-lines-on-p
        (page-break-lines-mode 1)))

    (add-hook 'company-completion-started-hook 'fred-page-break-lines-disable)
    (add-hook 'company-after-completion-hook 'fred-page-break-lines-maybe-reenable)))

(provide 'init-company)
;;; init-company.el ends here
