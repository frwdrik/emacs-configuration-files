;;; init-projectile.el --- Use Projectile for navigation within projects -*- lexical-binding: t -*-
;;; Commentary:
;;; Code:

(use-package projectile
  :ensure t
  :diminish
  :hook (after-init . projectile-mode)
  :bind-keymap ("C-c p" . projectile-command-map)
  :config
  (setq-default projectile-mode-line-prefix " Proj")
  (setq-default projectile-project-search-path '("~/src/programming/")))
;; (maybe-require-package 'ibuffer-projectile)

(provide 'init-projectile)
;;; init-projectile.el ends here
