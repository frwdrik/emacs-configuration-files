;;; init.el --- Load the full configuration -*- lexical-binding: t -*-
;;; Commentary:

;; This file bootstraps the configuration, which is divided into
;; a number of other files. These configuration files are placed
;; in the folder "lisp" in this directory. 

;;; Code:

;; Produce backtraces when errors occur
(setq debug-on-error nil)

(let ((minver "24.4"))
  (when (version< emacs-version minver)
    (error "Your Emacs is too old -- this config requires v%s or higher" minver)))
(when (version< emacs-version "25.1")
  (message "Your Emacs is old, and some functionality in this config will be disabled. Please upgrade if possible."))

(add-to-list 'load-path (expand-file-name "lisp" user-emacs-directory))
(add-to-list 'load-path (expand-file-name "site-lisp" user-emacs-directory))

;;(require 'init-benchmarking) ;; Measure startup time

(defconst *spell-check-support-enabled* nil) ;; Enable with t if you prefer
(defconst *is-a-mac* (eq system-type 'darwin))

;;----------------------------------------------------------------------------
;; Adjust garbage collection thresholds during startup, and thereafter
;;----------------------------------------------------------------------------
(let ((normal-gc-cons-threshold (* 20 1024 1024))
      (init-gc-cons-threshold (* 512 1024 1024)))
  (setq gc-cons-threshold init-gc-cons-threshold)
  (add-hook 'emacs-startup-hook
            (lambda () (setq gc-cons-threshold normal-gc-cons-threshold))))

;;----------------------------------------------------------------------------
;; Adopt keyboard to a dvorak layout
;;----------------------------------------------------------------------------
;; (keyboard-translate ?\C-t ?\C-x)
;; (keyboard-translate ?\C-x ?\C-t) ; works perfect
(define-key key-translation-map  (kbd "C-x") (kbd "C-t")) 
(define-key key-translation-map  (kbd "C-t") (kbd "C-x")) ; works perfect

(global-set-key (kbd "C-x C-t") 'slime-eval-defun)
(global-set-key (kbd "C-&") 'undo)
(global-set-key (kbd "<H-backspace>") 'undo)
(global-set-key (kbd "C-c M-w") 'symbol-overlay-save-symbol)

;;----------------------------------------------------------------------------
;; Bootstrap config
;;----------------------------------------------------------------------------
;; store all backup and autosave files in the tmp dir
(setq backup-directory-alist
      `((".*" . ,temporary-file-directory)))
(setq auto-save-file-name-transforms
      `((".*" ,temporary-file-directory t)))
(setq create-lockfiles nil)

(setq custom-file (expand-file-name "custom.el" user-emacs-directory))
(load-file custom-file)
(require 'init-utils)
(require 'init-site-lisp) ;; Must come before elpa, as it may provide package.el
;; ;; Calls (package-initialize)
(require 'init-elpa)      ;; Machinery for installing required
(require 'init-exec-path) ;; Set up $PATH

;;----------------------------------------------------------------------------
;; Allow users to provide an optional "init-preload-local.el"
;;----------------------------------------------------------------------------
(require 'init-preload-local nil t)

;;----------------------------------------------------------------------------
;; Load configs for specific features and modes
;;----------------------------------------------------------------------------

(use-package diminish :ensure t)	; remove modeline display
(require-package 'scratch)		; mode-based scratch buffers
(require-package 'command-log-mode)	; create keystroke log

;;(require 'init-frame-hooks)
;; (require 'init-xterm)
(use-package init-themes)
;; (require 'init-osx-keys)
(use-package init-gui-frames)
(use-package init-dired)
(use-package init-isearch) 
(use-package init-grep)
(use-package init-deft)
(use-package init-uniquify)
(use-package init-ibuffer :bind (("C-x C-b" . ibuffer)))
(use-package init-flycheck :defer t)
(use-package init-recentf )
(use-package init-ivy)
(use-package init-hippie-expand)
(use-package init-company)
(use-package init-bash-completion)
(require 'init-windows)
(use-package init-sessions :defer t)
;;(use-package init-mmm)
(use-package init-popwin)
(use-package init-editing-utils)
(use-package init-whitespace)


;;; Version control
(use-package init-vc)
(use-package init-git)
;; (require 'init-github)
(use-package init-projectile)
;; (require 'init-neotree)
(require 'init-compile)
(require 'init-c)
(require 'init-m2)
(use-package init-latex)
(use-package init-yasnippet)
;; (require 'init-crontab)
;; (require 'init-textile)
;; (require 'init-markdown)
;; (require 'init-csv)
;; (require 'init-erlang)
;; (require 'init-javascript)
;; (require 'init-php)
(use-package init-org)
;; (require 'init-nxml)
;; (require 'init-html)
;; (require 'init-css)
;; (require 'init-haml)
;; (require 'init-http)
;; (require 'init-python)
(require 'init-haskell)
;; (require 'init-elm)
;; (require 'init-purescript)
;; (require 'init-ruby)
;; (require 'init-rails)
;; (require 'init-sql)
;; (require 'init-rust)
;; (require 'init-toml)
;; (require 'init-yaml)
;; (require 'init-docker)
;; (require 'init-terraform)
;; (require 'init-nix)
;; (maybe-require-package 'nginx-mode)

(use-package init-paredit)
(use-package init-lisp)
(use-package init-slime)
;; (require 'init-clojure)
;; (require 'init-clojure-cider)
(use-package init-common-lisp)

(require 'init-spelling)
;; (require 'init-folding)
(require 'init-misc)
(require 'init-custom-modes)

;;(require 'init-twitter)
;; (require 'init-mu)
;; (require 'init-ledger)


;; Extra packages which don't require any configuration

;; (require-package 'gnuplot)
;; (require-package 'lua-mode)
;; (require-package 'htmlize)
;; (require-package 'dsvn)

;;----------------------------------------------------------------------------
;; Allow access from emacsclient
;;----------------------------------------------------------------------------
;; (add-hook 'after-init-hook
;;           (lambda ()
;;             (require 'server)
;;             (unless (server-running-p)
;;               (server-start))))

;;----------------------------------------------------------------------------
;; Variables configured via the interactive 'customize' interface
;;----------------------------------------------------------------------------
(when (file-exists-p custom-file)
  (when (file-exists-p custom-file)
    (load custom-file)))


;;----------------------------------------------------------------------------
;; Locales (setting them earlier in this file doesn't work in X)
;;----------------------------------------------------------------------------
(require 'init-locales)


;;----------------------------------------------------------------------------
;; Allow users to provide an optional "init-local" containing personal settings
;;----------------------------------------------------------------------------
(require 'init-local nil t)

(provide 'init)

;; Local Variables:
;; coding: utf-8
;; no-byte-compile: t
;; End:
;;; init.el ends here
